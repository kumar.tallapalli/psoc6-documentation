===============================================
CY Secure Tools
===============================================

This Python package contains security tools for creating keys, creating certificates, signing user applications, and provisioning Cypress MCUs.  

.. raw:: html

   <a href="https://pypi.org/project/cysecuretools/" target="_blank">Please click hear to be taken to the project page on the Python Package Index.</a><br><br>
