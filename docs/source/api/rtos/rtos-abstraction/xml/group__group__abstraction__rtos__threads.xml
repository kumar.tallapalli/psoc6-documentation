<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__abstraction__rtos__threads" kind="group">
    <compoundname>group_abstraction_rtos_threads</compoundname>
    <title>Threads</title>
      <sectiondef kind="user-defined">
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__threads_1ga002d529ea362c3e7670cc04de1bdb30e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_create_thread</definition>
        <argsstring>(cy_thread_t *thread, cy_thread_entry_fn_t entry_function, const char *name, void *stack, uint32_t stack_size, cy_thread_priority_t priority, cy_thread_arg_t arg)</argsstring>
        <name>cy_rtos_create_thread</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga27c9e935c4c4f200d86a8484d3db08d3">cy_thread_t</ref> *</type>
          <declname>thread</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__threads_1ga1e3db7a2b7a64fe9838804f049612d43">cy_thread_entry_fn_t</ref></type>
          <declname>entry_function</declname>
        </param>
        <param>
          <type>const char *</type>
          <declname>name</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>stack</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>stack_size</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga9f21bb78897c25df019b6e4286ecc7a0">cy_thread_priority_t</ref></type>
          <declname>priority</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga38237cfc682f2212e1ae11eb81a594b4">cy_thread_arg_t</ref></type>
          <declname>arg</declname>
        </param>
        <briefdescription>
<para>Create a thread with specific thread argument. </para>        </briefdescription>
        <detaileddescription>
<para>This function is called to startup a new thread. If the thread can exit, it must call <ref kindref="member" refid="group__group__abstraction__rtos__threads_1ga2865c511e5f36874e1dc6cb6b0fc4f4b">cy_rtos_exit_thread()</ref> just before doing so. All created threads that can terminate, either by themselves or forcefully by another thread MUST have <ref kindref="member" refid="group__group__abstraction__rtos__threads_1ga0baae5db6b3b3c342fee92af38d4da7b">cy_rtos_join_thread()</ref> called on them by another thread in order to cleanup any resources that might have been allocated for them.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">thread</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a variable which will receive the new thread handle </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">entry_function</parametername>
</parameternamelist>
<parameterdescription>
<para>Function pointer which points to the main function for the new thread </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">name</parametername>
</parameternamelist>
<parameterdescription>
<para>String thread name used for a debugger </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">stack</parametername>
</parameternamelist>
<parameterdescription>
<para>The buffer to use for the thread stack. This must be aligned to <ref kindref="member" refid="group__group__abstraction__rtos__port_1ga03f4b97a8323f791fe6b8113bac20610">CY_RTOS_ALIGNMENT_MASK</ref> with a size of at least <ref kindref="member" refid="group__group__abstraction__rtos__port_1ga8cb68f0797eec0941d819cb03dee3f8e">CY_RTOS_MIN_STACK_SIZE</ref>. If stack is null, cy_rtos_create_thread will allocate a stack from the heap. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">stack_size</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the thread stack in bytes </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">priority</parametername>
</parameternamelist>
<parameterdescription>
<para>The priority of the thread. Values are operating system specific, but some common priority levels are defined: CY_THREAD_PRIORITY_LOW CY_THREAD_PRIORITY_NORMAL CY_THREAD_PRIORITY_HIGH </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">arg</parametername>
</parameternamelist>
<parameterdescription>
<para>The argument to pass to the new thread</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of thread create request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga516d500bef7d6e6291f08485d0bc003b">CY_RTOS_NO_MEMORY</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="198" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__threads_1ga2865c511e5f36874e1dc6cb6b0fc4f4b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_exit_thread</definition>
        <argsstring>()</argsstring>
        <name>cy_rtos_exit_thread</name>
        <briefdescription>
<para>Exit the current thread. </para>        </briefdescription>
        <detaileddescription>
<para>This function is called just before a thread exits. In some cases it is sufficient for a thread to just return to exit, but in other cases, the RTOS must be explicitly signaled. In cases where a return is sufficient, this should be a null funcition. where the RTOS must be signaled, this function should perform that In cases operation. In code using RTOS services, this function should be placed at any at any location where the main thread function will return, exiting the thread. Threads that can exit must still be joined (<ref kindref="member" refid="group__group__abstraction__rtos__threads_1ga0baae5db6b3b3c342fee92af38d4da7b">cy_rtos_join_thread</ref>) to ensure their resources are fully cleaned up.</para><para><simplesect kind="return"><para>The status of thread exit request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="214" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__threads_1ga738c6b5b507480b4dcc735be14eeb804" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_terminate_thread</definition>
        <argsstring>(cy_thread_t *thread)</argsstring>
        <name>cy_rtos_terminate_thread</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga27c9e935c4c4f200d86a8484d3db08d3">cy_thread_t</ref> *</type>
          <declname>thread</declname>
        </param>
        <briefdescription>
<para>Terminates another thread. </para>        </briefdescription>
        <detaileddescription>
<para>This function is called to terminate another thread and reap the resources claimed by the thread. This should be called both when forcibly terminating another thread as well as any time a thread can exit on its own. For some RTOS implementations this is not required as the thread resources are claimed as soon as it exits. In other cases, this must be called to reclaim resources. Threads that are terminated must still be joined (<ref kindref="member" refid="group__group__abstraction__rtos__threads_1ga0baae5db6b3b3c342fee92af38d4da7b">cy_rtos_join_thread</ref>) to ensure their resources are fully cleaned up.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">thread</parametername>
</parameternamelist>
<parameterdescription>
<para>Handle of the thread to terminate</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the thread terminate. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="230" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__threads_1ga0baae5db6b3b3c342fee92af38d4da7b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_join_thread</definition>
        <argsstring>(cy_thread_t *thread)</argsstring>
        <name>cy_rtos_join_thread</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga27c9e935c4c4f200d86a8484d3db08d3">cy_thread_t</ref> *</type>
          <declname>thread</declname>
        </param>
        <briefdescription>
<para>Waits for a thread to complete. </para>        </briefdescription>
        <detaileddescription>
<para>This must be called on any thread that can complete to ensure that any resources that were allocated for it are cleaned up.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">thread</parametername>
</parameternamelist>
<parameterdescription>
<para>Handle of the thread to wait for</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of thread join request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="241" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__threads_1ga4c65be97daa12d333854e1a79194b573" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_is_thread_running</definition>
        <argsstring>(cy_thread_t *thread, bool *running)</argsstring>
        <name>cy_rtos_is_thread_running</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga27c9e935c4c4f200d86a8484d3db08d3">cy_thread_t</ref> *</type>
          <declname>thread</declname>
        </param>
        <param>
          <type>bool *</type>
          <declname>running</declname>
        </param>
        <briefdescription>
<para>Checks if the thread is running. </para>        </briefdescription>
        <detaileddescription>
<para>This function is called to determine if a thread is actively running or not. For information on the thread state, use the <ref kindref="member" refid="group__group__abstraction__rtos__threads_1ga2a0ab5f6b16adadd4ea34365b4f73d58">cy_rtos_get_thread_state()</ref> function.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">thread</parametername>
</parameternamelist>
<parameterdescription>
<para>Handle of the terminated thread to delete </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">running</parametername>
</parameternamelist>
<parameterdescription>
<para>Returns true if the thread is running, otherwise false</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the thread running check. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="253" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__threads_1ga2a0ab5f6b16adadd4ea34365b4f73d58" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_get_thread_state</definition>
        <argsstring>(cy_thread_t *thread, cy_thread_state_t *state)</argsstring>
        <name>cy_rtos_get_thread_state</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga27c9e935c4c4f200d86a8484d3db08d3">cy_thread_t</ref> *</type>
          <declname>thread</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__threads_1gacc555d9fef9df1454dbcc8e3bd3e4ee6">cy_thread_state_t</ref> *</type>
          <declname>state</declname>
        </param>
        <briefdescription>
<para>Gets the state the thread is currently in. </para>        </briefdescription>
        <detaileddescription>
<para>This function is called to determine if a thread is running/blocked/inactive/ready etc.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">thread</parametername>
</parameternamelist>
<parameterdescription>
<para>Handle of the terminated thread to delete </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">state</parametername>
</parameternamelist>
<parameterdescription>
<para>Returns the state the thread is currently in</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the thread state check. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="264" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__abstraction__rtos__threads_1gac0d712da15dd0ee02f9b53dbca584e29" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t cy_rtos_get_thread_handle</definition>
        <argsstring>(cy_thread_t *thread)</argsstring>
        <name>cy_rtos_get_thread_handle</name>
        <param>
          <type><ref kindref="member" refid="group__group__abstraction__rtos__port_1ga27c9e935c4c4f200d86a8484d3db08d3">cy_thread_t</ref> *</type>
          <declname>thread</declname>
        </param>
        <briefdescription>
<para>Get current thread handle. </para>        </briefdescription>
        <detaileddescription>
<para>Returns the unique thread handle of the current running thread.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">thread</parametername>
</parameternamelist>
<parameterdescription>
<para>Handle of the current running thread</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of thread join request. [<ref kindref="member" refid="group__group__abstraction__rtos__common_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>, <ref kindref="member" refid="group__group__abstraction__rtos__common_1ga89a0a071bd7a41b5d1f3edb969ee1647">CY_RTOS_GENERAL_ERROR</ref>] </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="274" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="enum">
      <memberdef id="group__group__abstraction__rtos__threads_1gacc555d9fef9df1454dbcc8e3bd3e4ee6" kind="enum" prot="public" static="no" strong="no">
        <type />
        <name>cy_thread_state_t</name>
        <enumvalue id="group__group__abstraction__rtos__threads_1ggacc555d9fef9df1454dbcc8e3bd3e4ee6a19af6e196af4c8cbe3d162f070892877" prot="public">
          <name>CY_THREAD_STATE_INACTIVE</name>
          <briefdescription>
<para>thread has not started or was terminated but not yet joined </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__abstraction__rtos__threads_1ggacc555d9fef9df1454dbcc8e3bd3e4ee6a8efc8355c0ce19bbc62e1e19950afc94" prot="public">
          <name>CY_THREAD_STATE_READY</name>
          <briefdescription>
<para>thread can run, but is not currently </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__abstraction__rtos__threads_1ggacc555d9fef9df1454dbcc8e3bd3e4ee6a2b2a8db515615c92ad655cbb1ee3183e" prot="public">
          <name>CY_THREAD_STATE_RUNNING</name>
          <briefdescription>
<para>thread is currently running </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__abstraction__rtos__threads_1ggacc555d9fef9df1454dbcc8e3bd3e4ee6a1188aa9968e841190908389b93a28cc2" prot="public">
          <name>CY_THREAD_STATE_BLOCKED</name>
          <briefdescription>
<para>thread is blocked waiting for something </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__abstraction__rtos__threads_1ggacc555d9fef9df1454dbcc8e3bd3e4ee6aa5113cb861c5e7a6b1ee48f9eacf3b87" prot="public">
          <name>CY_THREAD_STATE_TERMINATED</name>
          <briefdescription>
<para>thread has terminated but not freed </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__abstraction__rtos__threads_1ggacc555d9fef9df1454dbcc8e3bd3e4ee6a858524ed8b50486ec25c0949e6c443fd" prot="public">
          <name>CY_THREAD_STATE_UNKNOWN</name>
          <briefdescription>
<para>thread is in an unknown state </para>          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <briefdescription>
<para><bold>cy_thread_state_t: </bold><linebreak />The state a thread can be in.</para></briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="122" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" bodystart="114" column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="115" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="typedef">
      <memberdef id="group__group__abstraction__rtos__threads_1ga1e3db7a2b7a64fe9838804f049612d43" kind="typedef" prot="public" static="no">
        <type>void(*</type>
        <definition>typedef void(* cy_thread_entry_fn_t) (cy_thread_arg_t arg)</definition>
        <argsstring>)(cy_thread_arg_t arg)</argsstring>
        <name>cy_thread_entry_fn_t</name>
        <briefdescription>
<para>The type of a function that is the entry point for a thread. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">arg</parametername>
</parameternamelist>
<parameterdescription>
<para>the argument passed from the thread create call to the entry function </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" bodystart="144" column="1" file="output/libs/COMPONENT_ABS_RTOS/abstraction-rtos/include/cyabs_rtos.h" line="144" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>APIs for creating and working with Threads. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>