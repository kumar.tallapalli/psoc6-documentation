============
HAL Drivers
============

.. doxygengroup:: group_hal
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::


   group__group__hal__dma.rst
   group__group__hal__adc.rst
   group__group__hal__clock.rst
   group__group__hal__comp.rst
   group__group__hal__crc.rst
   group__group__hal__dac.rst
   group__group__hal__ezi2c.rst
   group__group__hal__flash.rst
   group__group__hal__syspm.rst
   group__group__hal__gpio.rst
   group__group__hal__hwmgr.rst
   group__group__hal__i2c.rst
   group__group__hal__i2s.rst
   group__group__hal__interconnect.rst
   group__group__hal__lptimer.rst
   group__group__hal__opamp.rst
   group__group__hal__pdmpcm.rst
   group__group__hal__pwm.rst
   group__group__hal__qspi.rst
   group__group__hal__quaddec.rst
   group__group__hal__rtc.rst
   group__group__hal__sdhc.rst
   group__group__hal__sdio.rst
   group__group__hal__spi.rst
   group__group__hal__system.rst
   group__group__hal__timer.rst
   group__group__hal__trng.rst
   group__group__hal__uart.rst
   group__group__hal__usb__dev.rst
   roup__group__hal__wdt.rst
