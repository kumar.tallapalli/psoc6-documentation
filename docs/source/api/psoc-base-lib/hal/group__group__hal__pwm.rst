============================
PWM (Pulse Width Modulator)
============================

.. doxygengroup:: group_hal_pwm
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__pwm.rst
   

