<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__crc__impl_8h" kind="file" language="C++">
    <compoundname>cyhal_crc_impl.h</compoundname>
    <includes local="yes" refid="cyhal__crc_8h">cyhal_crc.h</includes>
    <includes local="yes" refid="cyhal__hwmgr_8h">cyhal_hwmgr.h</includes>
    <includes local="yes">cy_utils.h</includes>
    <includedby local="yes" refid="cyhal__crc_8c">cyhal_crc.c</includedby>
    <incdepgraph>
      <node id="53">
        <label>cyhal_crc_impl.h</label>
        <link refid="cyhal__crc__impl_8h" />
        <childnode refid="54" relation="include">
        </childnode>
      </node>
      <node id="54">
        <label>cy_utils.h</label>
      </node>
    </incdepgraph>
    <briefdescription>
<para>Description: Provides a high level interface for interacting with the Cypress CRC accelerator. </para>    </briefdescription>
    <detaileddescription>
<para>This is a wrapper around the lower level PDL API.</para><para><simplesect kind="copyright"><para>Copyright 2018-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="26"><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="27"><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_crc.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_hwmgr.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_utils.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_IP_MXCRYPTO)</highlight><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="36"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />__cplusplus<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /><highlight class="comment">//<sp />This<sp />helper<sp />function<sp />mirrors<sp />the<sp />definition<sp />of<sp />cyhal_crc_start</highlight><highlight class="normal" /></codeline>
<codeline lineno="39"><highlight class="normal">__STATIC_INLINE<sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />_cyhal_crc_start(<ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref><sp />*obj,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcrc__algorithm__t">crc_algorithm_t</ref><sp />*algorithm)</highlight></codeline>
<codeline lineno="40"><highlight class="normal">{</highlight></codeline>
<codeline lineno="41"><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(NULL<sp />!=<sp />obj);</highlight></codeline>
<codeline lineno="42"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(NULL<sp />==<sp />algorithm)</highlight></codeline>
<codeline lineno="43"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__hal__results__crc_1ga8f573a6a3660ae4086e9cde2da454324">CYHAL_CRC_RSLT_ERR_BAD_ARGUMENT</ref>;</highlight></codeline>
<codeline lineno="44"><highlight class="normal" /></codeline>
<codeline lineno="45"><highlight class="normal"><sp /><sp /><sp /><sp />obj-&gt;crc_width<sp />=<sp />algorithm-&gt;<ref kindref="member" refid="structcrc__algorithm__t_1a43f6798e59b2c14c20cf662a9c8952e6">width</ref>;</highlight></codeline>
<codeline lineno="46"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />Cy_Crypto_Core_Crc_CalcInit(obj-&gt;base,</highlight></codeline>
<codeline lineno="47"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />algorithm-&gt;<ref kindref="member" refid="structcrc__algorithm__t_1a43f6798e59b2c14c20cf662a9c8952e6">width</ref>,</highlight></codeline>
<codeline lineno="48"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />algorithm-&gt;<ref kindref="member" refid="structcrc__algorithm__t_1a75ef8f2fc1413a0d9fc0e6e54446e381">polynomial</ref>,</highlight></codeline>
<codeline lineno="49"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />algorithm-&gt;<ref kindref="member" refid="structcrc__algorithm__t_1a500da567a545d16ec7f1e7de51b97b6f">dataReverse</ref><sp />?<sp />1u<sp />:<sp />0u,</highlight></codeline>
<codeline lineno="50"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />algorithm-&gt;<ref kindref="member" refid="structcrc__algorithm__t_1aba877155c7581d729886913d945e9373">dataXor</ref>,</highlight></codeline>
<codeline lineno="51"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />algorithm-&gt;<ref kindref="member" refid="structcrc__algorithm__t_1a5ad81955519341e5b258a54559a4146f">remReverse</ref><sp />?<sp />1u<sp />:<sp />0u,</highlight></codeline>
<codeline lineno="52"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />algorithm-&gt;<ref kindref="member" refid="structcrc__algorithm__t_1a47e14b82b511b028131dc7bef2cc2a47">remXor</ref>,</highlight></codeline>
<codeline lineno="53"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />algorithm-&gt;<ref kindref="member" refid="structcrc__algorithm__t_1a764e5ee5b27363ed28ac4cca415a089e">lfsrInitState</ref>);</highlight></codeline>
<codeline lineno="54"><highlight class="normal">}</highlight></codeline>
<codeline lineno="55"><highlight class="normal" /></codeline>
<codeline lineno="56"><highlight class="normal" /><highlight class="preprocessor">#define<sp />cyhal_crc_start(obj,<sp />algorithm)<sp />_cyhal_crc_start(obj,<sp />algorithm)</highlight><highlight class="normal" /></codeline>
<codeline lineno="57"><highlight class="normal" /></codeline>
<codeline lineno="58"><highlight class="normal" /><highlight class="comment">//<sp />This<sp />helper<sp />function<sp />mirrors<sp />the<sp />definition<sp />of<sp />cyhal_crc_compute</highlight><highlight class="normal" /></codeline>
<codeline lineno="59"><highlight class="normal">__STATIC_INLINE<sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />_cyhal_crc_compute(</highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref><sp />*obj,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />uint8_t<sp />*data,<sp /></highlight><highlight class="keywordtype">size_t</highlight><highlight class="normal"><sp />length)</highlight></codeline>
<codeline lineno="60"><highlight class="normal">{</highlight></codeline>
<codeline lineno="61"><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(NULL<sp />!=<sp />obj);</highlight></codeline>
<codeline lineno="62"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(NULL<sp />==<sp />data<sp />||<sp />0<sp />==<sp />length)</highlight></codeline>
<codeline lineno="63"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__hal__results__crc_1ga8f573a6a3660ae4086e9cde2da454324">CYHAL_CRC_RSLT_ERR_BAD_ARGUMENT</ref>;</highlight></codeline>
<codeline lineno="64"><highlight class="normal" /></codeline>
<codeline lineno="65"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />Cy_Crypto_Core_Crc_CalcPartial(obj-&gt;base,<sp />data,<sp />length);</highlight></codeline>
<codeline lineno="66"><highlight class="normal">}</highlight></codeline>
<codeline lineno="67"><highlight class="normal" /></codeline>
<codeline lineno="68"><highlight class="normal" /><highlight class="preprocessor">#define<sp />cyhal_crc_compute(obj,<sp />data,<sp />length)<sp />_cyhal_crc_compute(obj,<sp />data,<sp />length)</highlight><highlight class="normal" /></codeline>
<codeline lineno="69"><highlight class="normal" /></codeline>
<codeline lineno="70"><highlight class="normal" /><highlight class="comment">//<sp />This<sp />helper<sp />function<sp />mirrors<sp />the<sp />definition<sp />of<sp />cyhal_crc_finish</highlight><highlight class="normal" /></codeline>
<codeline lineno="71"><highlight class="normal">__STATIC_INLINE<sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />_cyhal_crc_finish(</highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__crc__t">cyhal_crc_t</ref><sp />*obj,<sp />uint32_t<sp />*crc)</highlight></codeline>
<codeline lineno="72"><highlight class="normal">{</highlight></codeline>
<codeline lineno="73"><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(NULL<sp />!=<sp />obj);</highlight></codeline>
<codeline lineno="74"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(NULL<sp />==<sp />crc)</highlight></codeline>
<codeline lineno="75"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__hal__results__crc_1ga8f573a6a3660ae4086e9cde2da454324">CYHAL_CRC_RSLT_ERR_BAD_ARGUMENT</ref>;</highlight></codeline>
<codeline lineno="76"><highlight class="normal" /></codeline>
<codeline lineno="77"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />Cy_Crypto_Core_Crc_CalcFinish(obj-&gt;base,<sp />obj-&gt;crc_width,<sp />crc);</highlight></codeline>
<codeline lineno="78"><highlight class="normal">}</highlight></codeline>
<codeline lineno="79"><highlight class="normal" /></codeline>
<codeline lineno="80"><highlight class="normal" /><highlight class="preprocessor">#define<sp />cyhal_crc_finish(obj,<sp />crc)<sp />_cyhal_crc_finish(obj,<sp />crc)</highlight><highlight class="normal" /></codeline>
<codeline lineno="81"><highlight class="normal" /></codeline>
<codeline lineno="82"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="83"><highlight class="normal">}</highlight></codeline>
<codeline lineno="84"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />__cplusplus<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="85"><highlight class="normal" /></codeline>
<codeline lineno="86"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />defined(CY_IP_MXCRYPTO)<sp />*/</highlight><highlight class="preprocessor" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_crc_impl.h" />
  </compounddef>
</doxygen>