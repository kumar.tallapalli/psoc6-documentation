<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__hal__opamp" kind="group">
    <compoundname>group_hal_opamp</compoundname>
    <title>Opamp (Operational Amplifier)</title>
    <innergroup refid="group__group__hal__results__opamp">Opamp HAL Results</innergroup>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__hal__opamp_1ga541bc16bacffaddecbc3af24bfb77256" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_opamp_init</definition>
        <argsstring>(cyhal_opamp_t *obj, cyhal_gpio_t vin_p, cyhal_gpio_t vin_m, cyhal_gpio_t vout)</argsstring>
        <name>cyhal_opamp_init</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__opamp__t">cyhal_opamp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref></type>
          <declname>vin_p</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref></type>
          <declname>vin_m</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref></type>
          <declname>vout</declname>
        </param>
        <briefdescription>
<para>Initialize the opamp peripheral. </para>        </briefdescription>
        <detaileddescription>
<para>If vin_m is <ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__04__80__tqfp_1gga43c4aa9a5556b7386cb604f0ab3b242ca3dbd1016ea99d087d747530418b89a01">NC</ref>, the opamp will be initialized in follower mode (unity gain).</para><para>The opamp will be initialized but not powered-on until <ref kindref="member" refid="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a">cyhal_opamp_set_power</ref> is called.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to an opamp object. The caller must allocate the memory for this object but the init function will initialize its contents. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">vin_p</parametername>
</parameternamelist>
<parameterdescription>
<para>Non-inverting input </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">vin_m</parametername>
</parameternamelist>
<parameterdescription>
<para>Inverting input </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">vout</parametername>
</parameternamelist>
<parameterdescription>
<para>opamp output </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the init request </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_opamp.h" line="101" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__opamp_1gae1ed8b252f2b7415021984996826a52b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void cyhal_opamp_free</definition>
        <argsstring>(cyhal_opamp_t *obj)</argsstring>
        <name>cyhal_opamp_free</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__opamp__t">cyhal_opamp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Deinitialize the opamp peripheral and free associated resources. </para>        </briefdescription>
        <detaileddescription>
<para>This will disconnect all inputs and outputs, including internal feedback.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>The opamp object </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_opamp.h" line="109" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref></type>
        <definition>cy_rslt_t cyhal_opamp_set_power</definition>
        <argsstring>(cyhal_opamp_t *obj, cyhal_power_level_t power)</argsstring>
        <name>cyhal_opamp_set_power</name>
        <param>
          <type><ref kindref="compound" refid="structcyhal__opamp__t">cyhal_opamp_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__hal__general__types_1ga246fde0e79029f514ce3846b31e366d7">cyhal_power_level_t</ref></type>
          <declname>power</declname>
        </param>
        <briefdescription>
<para>Changes the current operating power level of the opamp. </para>        </briefdescription>
        <detaileddescription>
<para>If the power level is set to <ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a0f601c5d5843c59e58ee1988dd0e6861">CYHAL_POWER_LEVEL_OFF</ref>, the opamp will be powered-off but it will retain its configuration, so it is not necessary to reconfigure it when changing the power level from <ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a0f601c5d5843c59e58ee1988dd0e6861">CYHAL_POWER_LEVEL_OFF</ref> to any other value.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Opamp object </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">power</parametername>
</parameternamelist>
<parameterdescription>
<para>The power level to set </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The status of the set power request </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="output/libs/COMPONENT_DEPRECATED/psoc6hal/include/cyhal_opamp.h" line="121" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>High level interface for interacting with the Operational Amplifier (Opamp). </para>    </briefdescription>
    <detaileddescription>

<para><heading level="1">Features</heading></para>
<para>Each opamp can operate in one of two modes:<itemizedlist>
<listitem><para>Opamp: Bare opamp with two input pins.</para></listitem><listitem><para>Follower: Also known as unity gain. Buffers the signal on a single input pin and drives the same voltage on the output.</para></listitem></itemizedlist>
</para><para>In both modes, the output is driven off chip via another pin.</para>

<para><heading level="1">Quickstart</heading></para>
<para>Call <ref kindref="member" refid="group__group__hal__opamp_1ga541bc16bacffaddecbc3af24bfb77256">cyhal_opamp_init</ref> to initialize an opamp instance by providing the opamp object (<bold>obj</bold>), non-inverting input pin (<bold>vin_p</bold>), inverting input pin (<bold>vin_m</bold>), and output pin (<bold>vout</bold>). If follower mode is desired, pass NC for <bold>vin_m</bold>.</para><para>Use <ref kindref="member" refid="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a">cyhal_opamp_set_power</ref> to configure the opamp power.</para>

<para><heading level="1">Code Snippets</heading></para>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Error checking is omitted for clarity &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>

<para><heading level="1">Snippet 1: Bare opamp initialization</heading></para>
<para>The following snippet initializes a bare opamp. Note that any passive components (e.g. resistive feedback) must be implemented off-chip. <programlisting filename="opamp.c"><codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />rslt;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__opamp__t">cyhal_opamp_t</ref><sp />opamp_obj;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />opamp,<sp />using<sp />pin<sp />P9_0<sp />for<sp />the<sp />non-inverting<sp />input,<sp />pin<sp />P9_1<sp />for<sp />the<sp />inverting</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />input,<sp />and<sp />P9_2<sp />for<sp />the<sp />output</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__opamp_1ga541bc16bacffaddecbc3af24bfb77256">cyhal_opamp_init</ref>(&amp;opamp_obj,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51ac556dc6692d40142383b444a0545fe6d">P9_0</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a95e8336ec074bbd5096a3c9e2195a514">P9_1</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a39b31257ba13f758407bb87aa9d02526">P9_2</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Power<sp />on<sp />the<sp />opamp<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a">cyhal_opamp_set_power</ref>(&amp;opamp_obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a6f0f4df45e9cac012a2c8e5ba736241b">CYHAL_POWER_LEVEL_DEFAULT</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Release<sp />opamp<sp />object<sp />after<sp />use<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__opamp_1gae1ed8b252f2b7415021984996826a52b">cyhal_opamp_free</ref>(&amp;opamp_obj);</highlight></codeline>
</programlisting></para>

<para><heading level="1">Snippet 2: Opamp follower initialization</heading></para>
<para>The following snippet initializes an opamp as a follower. <programlisting filename="opamp.c"><codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />rslt;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__opamp__t">cyhal_opamp_t</ref><sp />opamp_obj;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />opamp<sp />as<sp />follower,<sp />using<sp />pin<sp />P9_0<sp />for<sp />the<sp />input<sp />and<sp />P9_2<sp />for<sp />the<sp />output<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__opamp_1ga541bc16bacffaddecbc3af24bfb77256">cyhal_opamp_init</ref>(&amp;opamp_obj,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51ac556dc6692d40142383b444a0545fe6d">P9_0</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a3dbd1016ea99d087d747530418b89a01">NC</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a39b31257ba13f758407bb87aa9d02526">P9_2</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Power<sp />on<sp />the<sp />opamp<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a">cyhal_opamp_set_power</ref>(&amp;opamp_obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a6f0f4df45e9cac012a2c8e5ba736241b">CYHAL_POWER_LEVEL_DEFAULT</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Release<sp />opamp<sp />object<sp />after<sp />use<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__opamp_1gae1ed8b252f2b7415021984996826a52b">cyhal_opamp_free</ref>(&amp;opamp_obj);</highlight></codeline>
</programlisting></para>

<para><heading level="1">Snippet 3: Opamp powering-off and on</heading></para>
<para>The following snippet demonstrates temporarily powering-off the opamp without freeing it. <programlisting filename="opamp.c"><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />This<sp />assumes<sp />that<sp />the<sp />opamp<sp />has<sp />already<sp />been<sp />initialized<sp />as<sp />shown<sp />in<sp />snippet<sp />1<sp />or<sp />2<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Power<sp />on<sp />the<sp />opamp<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a">cyhal_opamp_set_power</ref>(&amp;opamp_obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a6f0f4df45e9cac012a2c8e5ba736241b">CYHAL_POWER_LEVEL_DEFAULT</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a">cyhal_opamp_set_power</ref>(&amp;opamp_obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a0f601c5d5843c59e58ee1988dd0e6861">CYHAL_POWER_LEVEL_OFF</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />When<sp />the<sp />opamp<sp />is<sp />needed<sp />again,<sp />power<sp />it<sp />back<sp />on<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a">cyhal_opamp_set_power</ref>(&amp;opamp_obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a6f0f4df45e9cac012a2c8e5ba736241b">CYHAL_POWER_LEVEL_DEFAULT</ref>);</highlight></codeline>
</programlisting></para>
    </detaileddescription>
  </compounddef>
</doxygen>