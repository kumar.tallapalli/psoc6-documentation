<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__opamp_8c" kind="file" language="C++">
    <compoundname>cyhal_opamp.c</compoundname>
    <includes local="yes" refid="cyhal__opamp_8h">cyhal_opamp.h</includes>
    <includes local="yes" refid="cyhal__gpio_8h">cyhal_gpio.h</includes>
    <includes local="yes" refid="cyhal__analog__common_8h">cyhal_analog_common.h</includes>
    <includes local="yes" refid="cyhal__hwmgr_8h">cyhal_hwmgr.h</includes>
    <includes local="yes">cy_ctb.h</includes>
    <incdepgraph>
      <node id="179">
        <label>cyhal_opamp.c</label>
        <link refid="cyhal__opamp_8c" />
        <childnode refid="180" relation="include">
        </childnode>
      </node>
      <node id="180">
        <label>cy_ctb.h</label>
      </node>
    </incdepgraph>
    <briefdescription>
<para><linebreak />
 Provides a high level interface for interacting with the Cypress Analog/Digital convert. </para>    </briefdescription>
    <detaileddescription>
<para>This interface abstracts out the chip specific details. If any chip specific functionality is necessary, or performance is critical the low level functions can be used directly.</para><para><simplesect kind="copyright"><para>Copyright 2018-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight><highlight class="normal"><sp /></highlight></codeline>
<codeline lineno="28"><highlight class="preprocessor">#include<sp />"cyhal_opamp.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_gpio.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_analog_common.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_hwmgr.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_ctb.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal" /></codeline>
<codeline lineno="49"><highlight class="preprocessor">#if<sp />defined(CY_IP_MXS40PASS_CTB_INSTANCES)</highlight><highlight class="normal" /></codeline>
<codeline lineno="50"><highlight class="normal" /></codeline>
<codeline lineno="51"><highlight class="normal" /><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />cy_stc_ctb_opamp_config_t<sp />cyhal_opamp_default_config<sp />=</highlight></codeline>
<codeline lineno="52"><highlight class="normal">{</highlight></codeline>
<codeline lineno="53"><highlight class="normal"><sp /><sp /><sp /><sp />.deepSleep<sp /><sp /><sp /><sp />=<sp />CY_CTB_DEEPSLEEP_ENABLE,</highlight></codeline>
<codeline lineno="54"><highlight class="normal"><sp /><sp /><sp /><sp />.oaPower<sp /><sp /><sp /><sp /><sp /><sp />=<sp />CY_CTB_POWER_OFF,</highlight></codeline>
<codeline lineno="55"><highlight class="normal"><sp /><sp /><sp /><sp />.oaMode<sp /><sp /><sp /><sp /><sp /><sp /><sp />=<sp />CY_CTB_MODE_OPAMP10X,</highlight></codeline>
<codeline lineno="56"><highlight class="normal"><sp /><sp /><sp /><sp />.oaPump<sp /><sp /><sp /><sp /><sp /><sp /><sp />=<sp />CY_CTB_PUMP_ENABLE,</highlight></codeline>
<codeline lineno="57"><highlight class="normal"><sp /><sp /><sp /><sp />.oaCompEdge<sp /><sp /><sp />=<sp />CY_CTB_COMP_EDGE_DISABLE,</highlight></codeline>
<codeline lineno="58"><highlight class="normal"><sp /><sp /><sp /><sp />.oaCompLevel<sp /><sp />=<sp />CY_CTB_COMP_DSI_TRIGGER_OUT_LEVEL,</highlight></codeline>
<codeline lineno="59"><highlight class="normal"><sp /><sp /><sp /><sp />.oaCompBypass<sp />=<sp />CY_CTB_COMP_BYPASS_SYNC,</highlight></codeline>
<codeline lineno="60"><highlight class="normal"><sp /><sp /><sp /><sp />.oaCompHyst<sp /><sp /><sp />=<sp />CY_CTB_COMP_HYST_DISABLE,</highlight></codeline>
<codeline lineno="61"><highlight class="normal"><sp /><sp /><sp /><sp />.oaCompIntrEn<sp />=<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,</highlight></codeline>
<codeline lineno="62"><highlight class="normal">};</highlight></codeline>
<codeline lineno="63"><highlight class="normal" /></codeline>
<codeline lineno="64"><highlight class="normal" /><highlight class="comment">/*<sp />PCoC6<sp />supports<sp />only<sp />one<sp />CTB<sp />block<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="65"><highlight class="normal" /><highlight class="preprocessor">#if<sp />(CY_IP_MXS40PASS_CTB_INSTANCES<sp />&gt;<sp />1)</highlight><highlight class="normal" /></codeline>
<codeline lineno="66"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#error<sp />"Unhandled<sp />CTB<sp />count"</highlight><highlight class="normal" /></codeline>
<codeline lineno="67"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="68"><highlight class="normal" /></codeline>
<codeline lineno="69"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="70"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal" /></codeline>
<codeline lineno="71"><highlight class="normal">{</highlight></codeline>
<codeline lineno="72"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="73"><highlight class="normal" /></codeline>
<codeline lineno="74"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp /><ref kindref="member" refid="group__group__hal__opamp_1ga541bc16bacffaddecbc3af24bfb77256">cyhal_opamp_init</ref>(<ref kindref="compound" refid="structcyhal__opamp__t">cyhal_opamp_t</ref><sp />*obj,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />vin_p,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />vin_m,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />vout)</highlight></codeline>
<codeline lineno="75"><highlight class="normal">{</highlight></codeline>
<codeline lineno="76"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Check<sp />if<sp />obj<sp />is<sp />free<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="77"><highlight class="normal"><sp /><sp /><sp /><sp />CY_ASSERT(NULL<sp />!=<sp />obj);</highlight></codeline>
<codeline lineno="78"><highlight class="normal" /></codeline>
<codeline lineno="79"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initial<sp />values<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="80"><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>;</highlight></codeline>
<codeline lineno="81"><highlight class="normal"><sp /><sp /><sp /><sp />memset(obj,<sp />0,<sp /></highlight><highlight class="keyword">sizeof</highlight><highlight class="normal">(<ref kindref="compound" refid="structcyhal__opamp__t">cyhal_opamp_t</ref>));</highlight></codeline>
<codeline lineno="82"><highlight class="normal"><sp /><sp /><sp /><sp />obj-&gt;base<sp />=<sp />NULL;</highlight></codeline>
<codeline lineno="83"><highlight class="normal"><sp /><sp /><sp /><sp />obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1ad111187a65d182386ea96858369b83d4">type</ref><sp />=<sp /><ref kindref="member" refid="group__group__hal__impl__hw__types_1gga63b023ea7b5e73db59ddc0423c77975baf4c05e33bb994cc6a1d94bd301dcc988">CYHAL_RSC_INVALID</ref>;</highlight></codeline>
<codeline lineno="84"><highlight class="normal"><sp /><sp /><sp /><sp />obj-&gt;is_init_success<sp />=<sp /></highlight><highlight class="keyword">false</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="85"><highlight class="normal" /></codeline>
<codeline lineno="86"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Validate<sp />input<sp />pins.<sp />vin_p<sp />and<sp />vout<sp />are<sp />mandatory<sp />pins,<sp />vin_m<sp />is<sp />optional.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="87"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />((<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a3dbd1016ea99d087d747530418b89a01">NC</ref><sp />==<sp />vin_p)<sp />||<sp />(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a3dbd1016ea99d087d747530418b89a01">NC</ref><sp />==<sp />vout))</highlight></codeline>
<codeline lineno="88"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="89"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__hal__results__opamp_1ga6bafe913b654804fd03468d8780fb477">CYHAL_OPAMP_RSLT_ERR_INVALID_PIN</ref>;</highlight></codeline>
<codeline lineno="90"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="91"><highlight class="normal" /></codeline>
<codeline lineno="92"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Allocate<sp />resources<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="93"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref><sp />==<sp />result)</highlight></codeline>
<codeline lineno="94"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="95"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp />_cyhal_opamp_init_common(&amp;(obj-&gt;resource),<sp /><ref kindref="member" refid="group__group__hal__results__opamp_1ga509cfa90c07896de1f6a8ee261619978">CYHAL_OPAMP_RSLT_BAD_ARGUMENT</ref>,<sp />vin_p,<sp />vin_m,<sp />vout,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gga707195ce0627016bf371643bdd9caa51a3dbd1016ea99d087d747530418b89a01">NC</ref><sp /></highlight><highlight class="comment">/*<sp />comp_out<sp />unused<sp />by<sp />opamp<sp />*/</highlight><highlight class="normal">);</highlight></codeline>
<codeline lineno="96"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="97"><highlight class="normal" /></codeline>
<codeline lineno="98"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Configure<sp />the<sp />opamp<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="99"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(result<sp />==<sp /><ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>)</highlight></codeline>
<codeline lineno="100"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="101"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />obj-&gt;pin_vin_p<sp />=<sp />vin_p;</highlight></codeline>
<codeline lineno="102"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />obj-&gt;pin_vin_m<sp />=<sp />vin_m;</highlight></codeline>
<codeline lineno="103"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />obj-&gt;pin_vout<sp />=<sp />vout;</highlight></codeline>
<codeline lineno="104"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />obj-&gt;base<sp />=<sp />_cyhal_ctb_base[(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1a91895766b5f5efe1dfffac2d7537e73a">block_num</ref><sp />/<sp />_CYHAL_OPAMP_PER_CTB)];</highlight></codeline>
<codeline lineno="105"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />result<sp />=<sp />Cy_CTB_OpampInit(obj-&gt;base,<sp />_cyhal_opamp_convert_sel(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1a91895766b5f5efe1dfffac2d7537e73a">block_num</ref>),<sp />&amp;cyhal_opamp_default_config);</highlight></codeline>
<codeline lineno="106"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />obj-&gt;is_init_success<sp />=<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="107"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="108"><highlight class="normal" /></codeline>
<codeline lineno="109"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(result<sp />==<sp /><ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>)</highlight></codeline>
<codeline lineno="110"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="111"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />programmable<sp />analog<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="112"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cyhal_analog_ctb_init(obj-&gt;base);</highlight></codeline>
<codeline lineno="113"><highlight class="normal" /></codeline>
<codeline lineno="114"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />OPAMP<sp />Routing.<sp />Close<sp />input<sp />switches<sp />for<sp />OA0<sp />or<sp />OA1.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="115"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Cy_CTB_SetAnalogSwitch(obj-&gt;base,<sp />_cyhal_opamp_convert_switch(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1a91895766b5f5efe1dfffac2d7537e73a">block_num</ref>),<sp />_cyhal_opamp_pin_to_mask(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1a91895766b5f5efe1dfffac2d7537e73a">block_num</ref><sp />%<sp />_CYHAL_OPAMP_PER_CTB,<sp />vin_p,<sp />vin_m,<sp />vout),<sp />CY_CTB_SWITCH_CLOSE);</highlight></codeline>
<codeline lineno="116"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_cyhal_opamp_set_isolation_switch(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1a91895766b5f5efe1dfffac2d7537e73a">block_num</ref><sp />%<sp />_CYHAL_OPAMP_PER_CTB,<sp />obj-&gt;base,<sp />CY_CTB_SWITCH_CLOSE);</highlight></codeline>
<codeline lineno="117"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />obj-&gt;is_init_success<sp />=<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="118"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="119"><highlight class="normal" /></codeline>
<codeline lineno="120"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Free<sp />OPAMP<sp />in<sp />case<sp />of<sp />failure<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="121"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(result<sp />!=<sp /><ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>)</highlight></codeline>
<codeline lineno="122"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="123"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__opamp_1gae1ed8b252f2b7415021984996826a52b">cyhal_opamp_free</ref>(obj);</highlight></codeline>
<codeline lineno="124"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="125"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />result;</highlight></codeline>
<codeline lineno="126"><highlight class="normal">}</highlight></codeline>
<codeline lineno="127"><highlight class="normal" /></codeline>
<codeline lineno="128"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp /><ref kindref="member" refid="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a">cyhal_opamp_set_power</ref>(<ref kindref="compound" refid="structcyhal__opamp__t">cyhal_opamp_t</ref><sp />*obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1ga246fde0e79029f514ce3846b31e366d7">cyhal_power_level_t</ref><sp />power)</highlight></codeline>
<codeline lineno="129"><highlight class="normal">{</highlight></codeline>
<codeline lineno="130"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Safe<sp />convert<sp />power<sp />level<sp />from<sp />HAL<sp />(cyhal_power_level_t)<sp />to<sp />PDL<sp />(cy_en_ctb_power_t)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="131"><highlight class="normal"><sp /><sp /><sp /><sp />cy_en_ctb_power_t<sp />power_level<sp />=<sp />(cy_en_ctb_power_t)_cyhal_opamp_convert_power(power);</highlight></codeline>
<codeline lineno="132"><highlight class="normal" /></codeline>
<codeline lineno="133"><highlight class="normal"><sp /><sp /><sp /><sp />Cy_CTB_SetPower(obj-&gt;base,<sp />_cyhal_opamp_convert_sel(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1a91895766b5f5efe1dfffac2d7537e73a">block_num</ref>),<sp />power_level,<sp />CY_CTB_PUMP_ENABLE);</highlight></codeline>
<codeline lineno="134"><highlight class="normal" /></codeline>
<codeline lineno="135"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__result_1gaf58fac450d9fff4472f03ad68f6e546e">CY_RSLT_SUCCESS</ref>;</highlight></codeline>
<codeline lineno="136"><highlight class="normal">}</highlight></codeline>
<codeline lineno="137"><highlight class="normal" /></codeline>
<codeline lineno="138"><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__hal__opamp_1gae1ed8b252f2b7415021984996826a52b">cyhal_opamp_free</ref>(<ref kindref="compound" refid="structcyhal__opamp__t">cyhal_opamp_t</ref><sp />*obj)</highlight></codeline>
<codeline lineno="139"><highlight class="normal">{</highlight></codeline>
<codeline lineno="140"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(NULL<sp />!=<sp />obj<sp />&amp;&amp;<sp />NULL<sp />!=<sp />obj-&gt;base)</highlight></codeline>
<codeline lineno="141"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="142"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(obj-&gt;is_init_success<sp />&amp;&amp;<sp />(!<ref kindref="member" refid="group__group__hal__opamp_1ga3d5784bf2463ccdb9c80a2a02d100f8a">cyhal_opamp_set_power</ref>(obj,<sp /><ref kindref="member" refid="group__group__hal__general__types_1gga246fde0e79029f514ce3846b31e366d7a0f601c5d5843c59e58ee1988dd0e6861">CYHAL_POWER_LEVEL_OFF</ref>)))</highlight></codeline>
<codeline lineno="143"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="144"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Cy_CTB_SetAnalogSwitch(obj-&gt;base,<sp />_cyhal_opamp_convert_switch(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1a91895766b5f5efe1dfffac2d7537e73a">block_num</ref>),<sp />_cyhal_opamp_pin_to_mask(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1a91895766b5f5efe1dfffac2d7537e73a">block_num</ref><sp />%<sp />_CYHAL_OPAMP_PER_CTB,<sp />obj-&gt;pin_vin_p,<sp />obj-&gt;pin_vin_m,<sp />obj-&gt;pin_vout),<sp />CY_CTB_SWITCH_OPEN);</highlight></codeline>
<codeline lineno="145"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_cyhal_opamp_set_isolation_switch(obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1a91895766b5f5efe1dfffac2d7537e73a">block_num</ref><sp />%<sp />_CYHAL_OPAMP_PER_CTB,<sp />obj-&gt;base,<sp />CY_CTB_SWITCH_OPEN);</highlight></codeline>
<codeline lineno="146"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cyhal_analog_ctb_free(obj-&gt;base);</highlight></codeline>
<codeline lineno="147"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="148"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="149"><highlight class="normal" /></codeline>
<codeline lineno="150"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(<ref kindref="member" refid="group__group__hal__impl__hw__types_1gga63b023ea7b5e73db59ddc0423c77975baf4c05e33bb994cc6a1d94bd301dcc988">CYHAL_RSC_INVALID</ref><sp />!=<sp />obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1ad111187a65d182386ea96858369b83d4">type</ref>)</highlight></codeline>
<codeline lineno="151"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="152"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__hwmgr_1gaa1979bd596d90251c7b861a7e94b884f">cyhal_hwmgr_free</ref>(&amp;obj-&gt;resource);</highlight></codeline>
<codeline lineno="153"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />obj-&gt;base<sp />=<sp />NULL;</highlight></codeline>
<codeline lineno="154"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />obj-&gt;resource.<ref kindref="member" refid="structcyhal__resource__inst__t_1ad111187a65d182386ea96858369b83d4">type</ref><sp />=<sp /><ref kindref="member" refid="group__group__hal__impl__hw__types_1gga63b023ea7b5e73db59ddc0423c77975baf4c05e33bb994cc6a1d94bd301dcc988">CYHAL_RSC_INVALID</ref>;</highlight></codeline>
<codeline lineno="155"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="156"><highlight class="normal" /></codeline>
<codeline lineno="157"><highlight class="normal"><sp /><sp /><sp /><sp />_cyhal_utils_release_if_used(&amp;(obj-&gt;pin_vin_p));</highlight></codeline>
<codeline lineno="158"><highlight class="normal"><sp /><sp /><sp /><sp />_cyhal_utils_release_if_used(&amp;(obj-&gt;pin_vout));</highlight></codeline>
<codeline lineno="159"><highlight class="normal"><sp /><sp /><sp /><sp />_cyhal_utils_release_if_used(&amp;(obj-&gt;pin_vin_m));</highlight></codeline>
<codeline lineno="160"><highlight class="normal">}</highlight></codeline>
<codeline lineno="161"><highlight class="normal" /></codeline>
<codeline lineno="162"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="163"><highlight class="normal">}</highlight></codeline>
<codeline lineno="164"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="165"><highlight class="normal" /></codeline>
<codeline lineno="166"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />CY_IP_MXS40PASS_CTB_INSTANCES<sp />*/</highlight><highlight class="preprocessor" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/source/cyhal_opamp.c" />
  </compounddef>
</doxygen>