==============
API Reference
==============

.. toctree::
   :hidden:

   group__group__result.rst
   group__group__utils.rst


The following provides a list of API documentation

+----------------------------------+----------------------------------+
|  `Result Type                    | Defines a type and related       |
|  <group__group__result.html>`_   | utilities for function result    |
|                                  | handling                         |
+----------------------------------+----------------------------------+
| `Utilities                       | Basic utility macros and         |
| <group__group__utils.html>`_     | functions                        |
+----------------------------------+----------------------------------+





