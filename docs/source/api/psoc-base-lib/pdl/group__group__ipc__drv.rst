==========================
IPC driver layer (IPC_DRV)
==========================

.. doxygengroup:: group_ipc_drv
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

.. toctree::

   group__group__ipc__macros.rst
   group__group__ipc__functions.rst
   group__group__ipc__data__structures.rst
   group__group__ipc__enums.rst
