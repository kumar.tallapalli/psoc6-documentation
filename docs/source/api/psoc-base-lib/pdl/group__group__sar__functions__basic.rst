===================================
Initialization and Basic Functions
===================================




.. doxygengroup:: group_sar_functions_basic
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: