================
Enumerated Types
================

.. doxygengroup:: group_gpio_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: