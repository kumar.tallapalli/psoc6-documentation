=======
Macros
=======

.. doxygengroup:: group_smartio_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


API Reference
^^^^^^^^^^^^^^

.. toctree::
   
   group__group__smartio__channels.rst