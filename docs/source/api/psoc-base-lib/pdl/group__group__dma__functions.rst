==========
Functions
==========

   
.. toctree::
       
   group__group__dma__block__functions.rst
   group__group__dma__channel__functions.rst
   group__group__dma__descriptor__functions.rst
   
.. doxygengroup:: group_dma_functions
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: