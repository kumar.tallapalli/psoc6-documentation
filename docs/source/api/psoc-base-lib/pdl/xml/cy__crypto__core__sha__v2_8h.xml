<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cy__crypto__core__sha__v2_8h" kind="file" language="C++">
    <compoundname>cy_crypto_core_sha_v2.h</compoundname>
    <includes local="yes" refid="cy__crypto__common_8h">cy_crypto_common.h</includes>
    <includedby local="yes" refid="cy__crypto__core__sha_8h">cy_crypto_core_sha.h</includedby>
    <includedby local="yes" refid="cy__crypto__core__hmac__v2_8c">cy_crypto_core_hmac_v2.c</includedby>
    <includedby local="yes" refid="cy__crypto__core__sha__v2_8c">cy_crypto_core_sha_v2.c</includedby>
    <briefdescription>
<para>This file provides constants and function prototypes for the API for the SHA method in the Crypto block driver. </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="version"><para>2.30.3</para></simplesect>
Copyright 2016-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para><para>Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at</para><para><ulink url="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org/licenses/LICENSE-2.0</ulink></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="27"><highlight class="preprocessor">#if<sp />!defined(CY_CRYPTO_CORE_SHA_V2_H)</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /><highlight class="preprocessor">#define<sp />CY_CRYPTO_CORE_SHA_V2_H</highlight><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_crypto_common.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_IP_MXCRYPTO)</highlight><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="36"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /><highlight class="preprocessor">#if<sp />(CPUSS_CRYPTO_SHA<sp />==<sp />1)</highlight><highlight class="normal" /></codeline>
<codeline lineno="39"><highlight class="normal" /></codeline>
<codeline lineno="42"><highlight class="keyword">typedef</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">struct</highlight><highlight class="normal" /></codeline>
<codeline lineno="43"><highlight class="normal">{</highlight></codeline>
<codeline lineno="44"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Allocates<sp />CRYPTO_MAX_BLOCK_SIZE<sp />Bytes<sp />for<sp />the<sp />block.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="45"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />block[<ref kindref="member" refid="group__group__crypto__cli__srv__macros_1ga766fcf287332f2f30eecf81cf6e143eb">CY_CRYPTO_SHA1_BLOCK_SIZE</ref><sp />/<sp />4u];</highlight></codeline>
<codeline lineno="46"><highlight class="normal" /></codeline>
<codeline lineno="47"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Allocates<sp />CRYPTO_MAX_HASH_SIZE<sp />Bytes<sp />for<sp />the<sp />hash.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="48"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />hash[CY_CRYPTO_SHA1_HASH_SIZE<sp />/<sp />4u];</highlight></codeline>
<codeline lineno="49"><highlight class="normal">}<sp />cy_stc_crypto_v2_sha1_buffers_t;</highlight></codeline>
<codeline lineno="50"><highlight class="normal" /></codeline>
<codeline lineno="51"><highlight class="normal" /><highlight class="keyword">typedef</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">struct</highlight><highlight class="normal" /></codeline>
<codeline lineno="52"><highlight class="normal">{</highlight></codeline>
<codeline lineno="53"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Allocates<sp />CRYPTO_MAX_BLOCK_SIZE<sp />Bytes<sp />for<sp />the<sp />block.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="54"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />block[<ref kindref="member" refid="group__group__crypto__cli__srv__macros_1ga2e492aa7682a4455db7ec6545f533354">CY_CRYPTO_SHA256_BLOCK_SIZE</ref><sp />/<sp />4u];</highlight></codeline>
<codeline lineno="55"><highlight class="normal" /></codeline>
<codeline lineno="56"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Allocates<sp />CRYPTO_MAX_HASH_SIZE<sp />Bytes<sp />for<sp />the<sp />hash.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="57"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />hash[CY_CRYPTO_SHA256_HASH_SIZE<sp />/<sp />4u];</highlight></codeline>
<codeline lineno="58"><highlight class="normal">}<sp />cy_stc_crypto_v2_sha256_buffers_t;</highlight></codeline>
<codeline lineno="59"><highlight class="normal" /></codeline>
<codeline lineno="60"><highlight class="normal" /><highlight class="keyword">typedef</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">struct</highlight><highlight class="normal" /></codeline>
<codeline lineno="61"><highlight class="normal">{</highlight></codeline>
<codeline lineno="62"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Allocates<sp />CRYPTO_MAX_BLOCK_SIZE<sp />Bytes<sp />for<sp />the<sp />block.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="63"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />block[<ref kindref="member" refid="group__group__crypto__cli__srv__macros_1gad3ac63b82ab093bc214c097c30773b21">CY_CRYPTO_SHA512_BLOCK_SIZE</ref><sp />/<sp />4u];</highlight></codeline>
<codeline lineno="64"><highlight class="normal" /></codeline>
<codeline lineno="65"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Allocates<sp />CRYPTO_MAX_HASH_SIZE<sp />Bytes<sp />for<sp />the<sp />hash.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="66"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />hash[CY_CRYPTO_SHA512_HASH_SIZE<sp />/<sp />4u];</highlight></codeline>
<codeline lineno="67"><highlight class="normal">}<sp />cy_stc_crypto_v2_sha512_buffers_t;</highlight></codeline>
<codeline lineno="68"><highlight class="normal" /></codeline>
<codeline lineno="69"><highlight class="normal"><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref><sp />Cy_Crypto_Core_V2_Sha_Init(CRYPTO_Type<sp />*base,</highlight></codeline>
<codeline lineno="70"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__crypto__sha__state__t">cy_stc_crypto_sha_state_t</ref><sp />*hashState,</highlight></codeline>
<codeline lineno="71"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__crypto__enums_1ga66ead9efce36e261e978cad722bf2dbb">cy_en_crypto_sha_mode_t</ref><sp />mode,</highlight></codeline>
<codeline lineno="72"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />*shaBuffers);</highlight></codeline>
<codeline lineno="73"><highlight class="normal" /></codeline>
<codeline lineno="74"><highlight class="normal"><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref><sp />Cy_Crypto_Core_V2_Sha_Start(CRYPTO_Type<sp />*base,<sp /><ref kindref="compound" refid="structcy__stc__crypto__sha__state__t">cy_stc_crypto_sha_state_t</ref><sp />*hashState);</highlight></codeline>
<codeline lineno="75"><highlight class="normal" /></codeline>
<codeline lineno="76"><highlight class="normal"><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref><sp />Cy_Crypto_Core_V2_Sha_Update(CRYPTO_Type<sp />*base,</highlight></codeline>
<codeline lineno="77"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__crypto__sha__state__t">cy_stc_crypto_sha_state_t</ref><sp />*hashState,</highlight></codeline>
<codeline lineno="78"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint8_t<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />*message,</highlight></codeline>
<codeline lineno="79"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp />messageSize);</highlight></codeline>
<codeline lineno="80"><highlight class="normal" /></codeline>
<codeline lineno="81"><highlight class="normal"><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref><sp />Cy_Crypto_Core_V2_Sha_Finish(CRYPTO_Type<sp />*base,</highlight></codeline>
<codeline lineno="82"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__crypto__sha__state__t">cy_stc_crypto_sha_state_t</ref><sp />*hashState,</highlight></codeline>
<codeline lineno="83"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint8_t<sp />*digest);</highlight></codeline>
<codeline lineno="84"><highlight class="normal" /></codeline>
<codeline lineno="85"><highlight class="normal"><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref><sp />Cy_Crypto_Core_V2_Sha_Free(CRYPTO_Type<sp />*base,<sp /><ref kindref="compound" refid="structcy__stc__crypto__sha__state__t">cy_stc_crypto_sha_state_t</ref><sp />*hashState);</highlight></codeline>
<codeline lineno="86"><highlight class="normal" /></codeline>
<codeline lineno="87"><highlight class="normal"><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref><sp />Cy_Crypto_Core_V2_Sha(CRYPTO_Type<sp />*base,</highlight></codeline>
<codeline lineno="88"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint8_t<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />*message,</highlight></codeline>
<codeline lineno="89"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint32_t<sp />messageSize,</highlight></codeline>
<codeline lineno="90"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />uint8_t<sp />*digest,</highlight></codeline>
<codeline lineno="91"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__crypto__enums_1ga66ead9efce36e261e978cad722bf2dbb">cy_en_crypto_sha_mode_t</ref><sp />mode);</highlight></codeline>
<codeline lineno="92"><highlight class="normal" /></codeline>
<codeline lineno="96"><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />#if<sp />(CPUSS_CRYPTO_SHA<sp />==<sp />1)<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="97"><highlight class="normal" /></codeline>
<codeline lineno="98"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="99"><highlight class="normal">}</highlight></codeline>
<codeline lineno="100"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="101"><highlight class="normal" /></codeline>
<codeline lineno="102"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />CY_IP_MXCRYPTO<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="103"><highlight class="normal" /></codeline>
<codeline lineno="104"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />#if<sp />!defined(CY_CRYPTO_CORE_SHA_V2_H)<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="105"><highlight class="normal" /></codeline>
<codeline lineno="106"><highlight class="normal" /></codeline>
<codeline lineno="107"><highlight class="normal" /><highlight class="comment">/*<sp />[]<sp />END<sp />OF<sp />FILE<sp />*/</highlight><highlight class="normal" /></codeline>
    </programlisting>
    <location file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_core_sha_v2.h" />
  </compounddef>
</doxygen>