<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ctdac__functions__basic" kind="group">
    <compoundname>group_ctdac_functions_basic</compoundname>
    <title>Basic Configuration Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__basic_1gad200575b1f2978c71744791e4a731d9b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTDAC_SetValue</definition>
        <argsstring>(CTDAC_Type *base, int32_t value)</argsstring>
        <name>Cy_CTDAC_SetValue</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>int32_t</type>
          <declname>value</declname>
        </param>
        <briefdescription>
<para>Set the CTDAC_VAL register (DAC hardware is updated on the next PeriClk cycle). </para>        </briefdescription>
        <detaileddescription>
<para>Only the least significant 12 bits have an effect. Sign extension of negative values is unnecessary and is ignored by the hardware. The way in which the CTDAC interprets the 12-bit data is controlled by <ref kindref="member" refid="group__group__ctdac__functions__basic_1ga4356463f9bf1c69c2afdfca69a60b850">Cy_CTDAC_SetSignMode</ref>.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Call this function only when the update mode is set to &lt;a href="#group__group__ctdac_1group_ctdac_updatemode_direct_write"&gt;Direct write&lt;/a&gt;. Calling this function for any other update mode will not have the intended effect.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>value</parametername>
</parameternamelist>
<parameterdescription>
<para>Value to write into the CTDAC_VAL register</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />update<sp />mode<sp />has<sp />been<sp />configured<sp />for<sp />Direct<sp />writes.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />To<sp />update<sp />the<sp />DAC<sp />value<sp />in<sp />this<sp />mode,<sp />Cy_CTDAC_SetValue()<sp />must<sp />be<sp />called.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Output<sp />Vref/2<sp />(unsigned<sp />mode).<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1gad200575b1f2978c71744791e4a731d9b">Cy_CTDAC_SetValue</ref>(CTDAC0,<sp />0x800);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />DAC<sp />output<sp />is<sp />updated<sp />on<sp />the<sp />next<sp />PeriClk<sp />cycle.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="798" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="795" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="683" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__basic_1ga1524565433728ffa19f773354b88f036" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTDAC_SetValueBuffered</definition>
        <argsstring>(CTDAC_Type *base, int32_t value)</argsstring>
        <name>Cy_CTDAC_SetValueBuffered</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>int32_t</type>
          <declname>value</declname>
        </param>
        <briefdescription>
<para>Set the CTDAC_VAL_NEXT register. </para>        </briefdescription>
        <detaileddescription>
<para>The value is transferred to the CTDAC_VAL register on the next edge of the CTDAC clock. Only the least significant 12 bits have an effect. Sign extension of negative values is unnecessary and is ignored by the hardware. The way in which the CTDAC interprets the 12-bit data is controlled by <ref kindref="member" refid="group__group__ctdac__functions__basic_1ga4356463f9bf1c69c2afdfca69a60b850">Cy_CTDAC_SetSignMode</ref>.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Calling this function in &lt;a href="#group__group__ctdac_1group_ctdac_updatemode_direct_write"&gt;Direct write&lt;/a&gt; mode will not update the DAC output. Call this function for all modes that use buffered values (i.e. uses a clock).&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>value</parametername>
</parameternamelist>
<parameterdescription>
<para>Value to write into the CTDAC_VAL_NEXT register</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />update<sp />mode<sp />has<sp />been<sp />configured<sp />for<sp />Buffered<sp />writes.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />To<sp />update<sp />the<sp />DAC<sp />value<sp />in<sp />this<sp />mode,<sp />a<sp />clock<sp />is<sp />required<sp />and</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Cy_CTDAC_SetValueBuffered()<sp />is<sp />used.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Output<sp />Vref/2<sp />(signed<sp />mode).<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1ga1524565433728ffa19f773354b88f036">Cy_CTDAC_SetValueBuffered</ref>(CTDAC0,<sp />0x000);<sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />rising<sp />edge<sp />of<sp />the<sp />DAC<sp />clock<sp />initiates<sp />an<sp />update<sp />to<sp />the<sp />DAC<sp />value.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Wait<sp />for<sp />an<sp />interrupt<sp />to<sp />signal<sp />that<sp />the<sp />DAC<sp />is<sp />ready<sp />for<sp />a<sp />new<sp />value.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="831" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="828" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="684" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__basic_1ga4356463f9bf1c69c2afdfca69a60b850" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTDAC_SetSignMode</definition>
        <argsstring>(CTDAC_Type *base, cy_en_ctdac_format_t formatMode)</argsstring>
        <name>Cy_CTDAC_SetSignMode</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctdac__enums_1ga52a4e5920c369f3cc51b3618668f02e0">cy_en_ctdac_format_t</ref></type>
          <declname>formatMode</declname>
        </param>
        <briefdescription>
<para>Set whether to interpret the DAC value as signed or unsigned. </para>        </briefdescription>
        <detaileddescription>
<para>In unsigned mode, the DAC value register is used without any decoding. In signed mode, the MSB is inverted by adding 0x800 to the DAC value. This converts the lowest signed number, 0x800, to the lowest unsigned number, 0x000.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>formatMode</parametername>
</parameternamelist>
<parameterdescription>
<para>Mode can be signed or unsigned. See <ref kindref="member" refid="group__group__ctdac__enums_1ga52a4e5920c369f3cc51b3618668f02e0">cy_en_ctdac_format_t</ref> for values.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />format<sp />of<sp />the<sp />DAC<sp />value<sp />register<sp />to<sp />be<sp />signed.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1ga4356463f9bf1c69c2afdfca69a60b850">Cy_CTDAC_SetSignMode</ref>(CTDAC0,<sp /><ref kindref="member" refid="group__group__ctdac__enums_1gga52a4e5920c369f3cc51b3618668f02e0ad7396b32a7ac35fe30197b985a323c38">CY_CTDAC_FORMAT_SIGNED</ref>);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="468" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctdac.c" bodystart="458" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="685" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__basic_1ga92374004a9bab9201916af6ee54c4f6d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTDAC_SetDeepSleepMode</definition>
        <argsstring>(CTDAC_Type *base, cy_en_ctdac_deep_sleep_t deepSleep)</argsstring>
        <name>Cy_CTDAC_SetDeepSleepMode</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctdac__enums_1ga08fecfbfc0d670966ae1aabe0db5ff2a">cy_en_ctdac_deep_sleep_t</ref></type>
          <declname>deepSleep</declname>
        </param>
        <briefdescription>
<para>Enable or disable the DAC hardware operation in Deep Sleep mode. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>deepSleep</parametername>
</parameternamelist>
<parameterdescription>
<para>Enable or disable Deep Sleep operation. Select value from <ref kindref="member" refid="group__group__ctdac__enums_1ga08fecfbfc0d670966ae1aabe0db5ff2a">cy_en_ctdac_deep_sleep_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />CTDAC<sp />is<sp />used<sp />as<sp />the<sp />reference<sp />voltage<sp />for<sp />a<sp />comparator</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />to<sp />wake<sp />up<sp />the<sp />device<sp />from<sp />Deep<sp />Sleep<sp />mode.<sp />The<sp />comparator<sp />has<sp />been<sp />configured</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />to<sp />operate<sp />in<sp />Deep<sp />Sleep<sp />mode.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Keep<sp />the<sp />CTDAC<sp />on<sp />in<sp />Deep<sp />Sleep<sp />mode.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1ga92374004a9bab9201916af6ee54c4f6d">Cy_CTDAC_SetDeepSleepMode</ref>(CTDAC0,<sp /><ref kindref="member" refid="group__group__ctdac__enums_1gga08fecfbfc0d670966ae1aabe0db5ff2aae31913053d33b3e08828568d5c3ac335">CY_CTDAC_DEEPSLEEP_ENABLE</ref>);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="498" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctdac.c" bodystart="489" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="686" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__basic_1gaf11a41cf842064d44f9f8cdaeea0a31e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTDAC_SetOutputMode</definition>
        <argsstring>(CTDAC_Type *base, cy_en_ctdac_output_mode_t outputMode)</argsstring>
        <name>Cy_CTDAC_SetOutputMode</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctdac__enums_1ga151d4aa27fabb95182a2494782994ccb">cy_en_ctdac_output_mode_t</ref></type>
          <declname>outputMode</declname>
        </param>
        <briefdescription>
<para>Set the output mode of the CTDAC: </para>        </briefdescription>
        <detaileddescription>
<para><itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ctdac__enums_1gga151d4aa27fabb95182a2494782994ccbaa3e1dfa33392b66a111cffdeb0df4527">CY_CTDAC_OUTPUT_HIGHZ</ref> : Disable the output</para></listitem><listitem><para><ref kindref="member" refid="group__group__ctdac__enums_1gga151d4aa27fabb95182a2494782994ccbaa0eedb42bfda13365d835dcc21ef33e6">CY_CTDAC_OUTPUT_VALUE</ref> : Enable the output and drive the value stored in the CTDAC_VAL register.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ctdac__enums_1gga151d4aa27fabb95182a2494782994ccba9d9e4e7c8dfcab9f6ef32358937709c8">CY_CTDAC_OUTPUT_VALUE_PLUS1</ref> : Enable the output and drive the value stored in the CTDAC_VAL register plus 1.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ctdac__enums_1gga151d4aa27fabb95182a2494782994ccba640a9cb727d264dc0dadae6586a2f8a8">CY_CTDAC_OUTPUT_VSSA</ref> : Output pulled to VSSA through 1.1 MOhm (typ) resistor.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ctdac__enums_1gga151d4aa27fabb95182a2494782994ccba2c8da77139224fb4e7f913cb48556f6d">CY_CTDAC_OUTPUT_VREF</ref> : Output pulled to VREF through 1.1 MOhm (typ) resistor.</para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>outputMode</parametername>
</parameternamelist>
<parameterdescription>
<para>Select a value from <ref kindref="member" refid="group__group__ctdac__enums_1ga151d4aa27fabb95182a2494782994ccb">cy_en_ctdac_output_mode_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />Sample<sp />and<sp />Hold<sp />(S/H)<sp />capacitor<sp />in<sp />the<sp />CTB<sp />block<sp />is<sp />used<sp />to</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />maintain<sp />the<sp />DAC<sp />output<sp />voltage<sp />for<sp />a<sp />duration<sp />of<sp />time<sp />so<sp />that</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />the<sp />DAC<sp />output<sp />can<sp />be<sp />turned<sp />off<sp />to<sp />save<sp />power.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Turn<sp />off<sp />DAC<sp />output.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1gaf11a41cf842064d44f9f8cdaeea0a31e">Cy_CTDAC_SetOutputMode</ref>(CTDAC0,<sp /><ref kindref="member" refid="group__group__ctdac__enums_1gga151d4aa27fabb95182a2494782994ccbaa3e1dfa33392b66a111cffdeb0df4527">CY_CTDAC_OUTPUT_HIGHZ</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />DAC<sp />output<sp />will<sp />need<sp />to<sp />turn<sp />on<sp />and<sp />sampled<sp />periodically<sp />to<sp />maintain</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />an<sp />acceptable<sp />voltage<sp />level<sp />across<sp />the<sp />S/H<sp />capacitor.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="536" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctdac.c" bodystart="526" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="687" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__basic_1gaa327e33f705cc55c86c9e4415d28b9ff" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTDAC_SetDeglitchMode</definition>
        <argsstring>(CTDAC_Type *base, cy_en_ctdac_deglitch_t deglitchMode)</argsstring>
        <name>Cy_CTDAC_SetDeglitchMode</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctdac__enums_1gaa5bbaaf21bda846a109573f84f1e2735">cy_en_ctdac_deglitch_t</ref></type>
          <declname>deglitchMode</declname>
        </param>
        <briefdescription>
<para>Enable deglitching on the unbuffered path, buffered path, both, or disable deglitching. </para>        </briefdescription>
        <detaileddescription>
<para>The deglitch mode should match the configured output path.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>deglitchMode</parametername>
</parameternamelist>
<parameterdescription>
<para>Deglitching mode selection. See <ref kindref="member" refid="group__group__ctdac__enums_1gaa5bbaaf21bda846a109573f84f1e2735">cy_en_ctdac_deglitch_t</ref> for values.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />CTDAC<sp />is<sp />configured<sp />to<sp />stay<sp />on<sp />in<sp />Deep<sp />Sleep<sp />mode.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Before<sp />entering<sp />Deep<sp />Sleep<sp />mode,<sp />the<sp />deglitch<sp />switches<sp />must<sp />be</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />turned<sp />off<sp />so<sp />that<sp />switches<sp />along<sp />the<sp />output<sp />path<sp />are<sp />guaranteed<sp />to<sp />be<sp />closed.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1gaa327e33f705cc55c86c9e4415d28b9ff">Cy_CTDAC_SetDeglitchMode</ref>(CTDAC0,<sp /><ref kindref="member" refid="group__group__ctdac__enums_1ggaa5bbaaf21bda846a109573f84f1e2735ac297e08904e92c27a89c93376be1c4af">CY_CTDAC_DEGLITCHMODE_NONE</ref>);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="568" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctdac.c" bodystart="558" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="688" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__basic_1ga1023b3cf97003859e12ced716a557d83" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTDAC_SetDeglitchCycles</definition>
        <argsstring>(CTDAC_Type *base, uint32_t deglitchCycles)</argsstring>
        <name>Cy_CTDAC_SetDeglitchCycles</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>deglitchCycles</declname>
        </param>
        <briefdescription>
<para>Set the number of deglitch cycles (0 to 63) that will be used. </para>        </briefdescription>
        <detaileddescription>
<para>To calculate the deglitch time: <verbatim>  (DEGLITCH_CNT + 1) / PERI_CLOCK_FREQ
</verbatim></para><para>The optimal deglitch time is 700 ns.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>deglitchCycles</parametername>
</parameternamelist>
<parameterdescription>
<para>Number of cycles to deglitch</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />PeriClk<sp />frequency<sp />has<sp />been<sp />changed<sp />during<sp />run-time.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Update<sp />the<sp />deglitch<sp />cycles<sp />so<sp />that<sp />the<sp />optimal<sp />deglitch<sp />time<sp />of<sp />700<sp />ns</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />can<sp />be<sp />maintained.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />PERICLK_FREQ_MHZ<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(50)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />DEGLITCH_TARGET_TIME_NS<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(700uL)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />FACTOR_NANO_TO_MICRO<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(1000uL)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />deglitchCycles<sp />=<sp />((PERICLK_FREQ_MHZ<sp />*<sp />DEGLITCH_TARGET_TIME_NS)<sp />/<sp />FACTOR_NANO_TO_MICRO)<sp />-<sp />1uL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1ga1023b3cf97003859e12ced716a557d83">Cy_CTDAC_SetDeglitchCycles</ref>(CTDAC0,<sp />deglitchCycles);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="603" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctdac.c" bodystart="594" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="689" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__basic_1ga3e645d5bdf428353fc09461b24a3352f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTDAC_SetRef</definition>
        <argsstring>(CTDAC_Type *base, cy_en_ctdac_ref_source_t refSource)</argsstring>
        <name>Cy_CTDAC_SetRef</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctdac__enums_1ga9c4416c55b96c81979223ec469eb7e0e">cy_en_ctdac_ref_source_t</ref></type>
          <declname>refSource</declname>
        </param>
        <briefdescription>
<para>Set the CTDAC reference source to Vdda or an external reference. </para>        </briefdescription>
        <detaileddescription>
<para>The external reference must come from Opamp1 of the CTB.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>refSource</parametername>
</parameternamelist>
<parameterdescription>
<para>The reference source. Select a value from <ref kindref="member" refid="group__group__ctdac__enums_1ga9c4416c55b96c81979223ec469eb7e0e">cy_en_ctdac_ref_source_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Change<sp />the<sp />reference<sp />source<sp />of<sp />the<sp />CTDAC<sp />to<sp />be<sp />VDDA</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />so<sp />that<sp />no<sp />buffers<sp />from<sp />the<sp />CTB<sp />are<sp />needed.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__basic_1ga3e645d5bdf428353fc09461b24a3352f">Cy_CTDAC_SetRef</ref>(CTDAC0,<sp /><ref kindref="member" refid="group__group__ctdac__enums_1gga9c4416c55b96c81979223ec469eb7e0ead259b9dd0e37537ea74be700fbe554f4">CY_CTDAC_REFSOURCE_VDDA</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="641" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctdac.c" bodystart="625" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="690" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This set of functions are for configuring basic usage of the CTDAC. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>