<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__ezi2c__slave__functions" kind="group">
    <compoundname>group_scb_ezi2c_slave_functions</compoundname>
    <title>Slave</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__ezi2c__slave__functions_1ga37de7020a41bff6ee0ad54af0a8be36e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_EZI2C_SetBuffer1</definition>
        <argsstring>(CySCB_Type const *base, uint8_t *buffer, uint32_t size, uint32_t rwBoundary, cy_stc_scb_ezi2c_context_t *context)</argsstring>
        <name>Cy_SCB_EZI2C_SetBuffer1</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>rwBoundary</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__ezi2c__context__t">cy_stc_scb_ezi2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Sets up the data buffer to be exposed to the I2C master on the primary slave address request. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the EZI2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the data buffer.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the buffer in bytes.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>rwBoundary</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of data bytes starting from the beginning of the buffer with Read and Write access. The data bytes located at rwBoundary or greater are read only.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__ezi2c__context__t">cy_stc_scb_ezi2c_context_t</ref> allocated by the user. The structure is used during the EZI2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;This function is not interrupt-protected and to prevent a race condition, it must be protected from the EZI2C interruption in the place where it is called.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Calling this function in the middle of a transaction intended for the secondary slave address leads to unexpected behavior. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="633" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_ezi2c.c" bodystart="621" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_ezi2c.h" line="394" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__ezi2c__slave__functions_1ga2435d7a2b8e0d069e6ae1f2686e64be8" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_EZI2C_SetBuffer2</definition>
        <argsstring>(CySCB_Type const *base, uint8_t *buffer, uint32_t size, uint32_t rwBoundary, cy_stc_scb_ezi2c_context_t *context)</argsstring>
        <name>Cy_SCB_EZI2C_SetBuffer2</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>buffer</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>size</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>rwBoundary</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__ezi2c__context__t">cy_stc_scb_ezi2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Sets up the data buffer to be exposed to the I2C master on the secondary slave address request. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the EZI2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>buffer</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the data buffer.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>size</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the buffer in bytes.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>rwBoundary</parametername>
</parameternamelist>
<parameterdescription>
<para>The number of data bytes starting from the beginning of the buffer with Read and Write access. The data bytes located at rwBoundary or greater are read only.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__ezi2c__context__t">cy_stc_scb_ezi2c_context_t</ref> allocated by the user. The structure is used during the EZI2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;This function is not interrupt-protected. To prevent a race condition, it must be protected from the EZI2C interruption in the place where it is called.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Calling this function in the middle of a transaction intended for the secondary slave address leads to unexpected behavior. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="744" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_ezi2c.c" bodystart="732" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_ezi2c.h" line="396" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__ezi2c__slave__functions_1ga8cd24e8c6c0bb996f6266443e1353f39" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_EZI2C_GetActivity</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_ezi2c_context_t *context)</argsstring>
        <name>Cy_SCB_EZI2C_GetActivity</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__ezi2c__context__t">cy_stc_scb_ezi2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns a non-zero value if an I2C Read or Write cycle has occurred since the last time this function was called. </para>        </briefdescription>
        <detaileddescription>
<para>All flags are reset to zero at the end of this function call, except the <ref kindref="member" refid="group__group__scb__ezi2c__macros__get__activity_1ga748ebd94817f62e09cf408412c54915b">CY_SCB_EZI2C_STATUS_BUSY</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the EZI2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__ezi2c__context__t">cy_stc_scb_ezi2c_context_t</ref> allocated by the user. The structure is used during the EZI2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__ezi2c__macros__get__activity">EZI2C Activity Status</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="524" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_ezi2c.c" bodystart="508" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_ezi2c.h" line="399" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__ezi2c__slave__functions_1ga5e811e428ecb264dddb284066d6ff956" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_EZI2C_Interrupt</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_ezi2c_context_t *context)</argsstring>
        <name>Cy_SCB_EZI2C_Interrupt</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__ezi2c__context__t">cy_stc_scb_ezi2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This is the interrupt function for the SCB configured in the EZI2C mode. </para>        </briefdescription>
        <detaileddescription>
<para>This function must be called inside the user-defined interrupt service routine to make the EZI2C slave work.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the EZI2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__ezi2c__context__t">cy_stc_scb_ezi2c_context_t</ref> allocated by the user. The structure is used during the EZI2C operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="839" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_ezi2c.c" bodystart="765" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_ezi2c.h" line="401" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>