<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__i2c__master__high__level__functions" kind="group">
    <compoundname>group_scb_i2c_master_high_level_functions</compoundname>
    <title>Master High-Level</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__high__level__functions_1ga91f7a32d2a20cf3deebe01cef8d9d000" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></type>
        <definition>cy_en_scb_i2c_status_t Cy_SCB_I2C_MasterWrite</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_i2c_master_xfer_config_t *xferConfig, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_MasterWrite</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__master__xfer__config__t">cy_stc_scb_i2c_master_xfer_config_t</ref> *</type>
          <declname>xferConfig</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function configures the master to automatically write an entire buffer of data to a slave device. </para>        </briefdescription>
        <detaileddescription>
<para>After the transaction is initiated by this function it returns and <ref kindref="member" refid="group__group__scb__i2c__interrupt__functions_1ga729b2fa4de4d44ea2e8995d7ca6a0c24">Cy_SCB_I2C_Interrupt</ref> manages further data transfer.</para><para>When a write transaction is completed (requested number of bytes are written or error occurred) the <ref kindref="member" refid="group__group__scb__i2c__macros__master__status_1ga412fef33336fb22c95f342ccf2485a0a">CY_SCB_I2C_MASTER_BUSY</ref> status is cleared and the <ref kindref="member" refid="group__group__scb__i2c__macros__callback__events_1gaf02c69c9cef5ecd87100df9d8ec9ea0f">CY_SCB_I2C_MASTER_WR_CMPLT_EVENT</ref> event is generated.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>xferConfig</parametername>
</parameternamelist>
<parameterdescription>
<para>Master transfer configuration structure <ref kindref="compound" refid="structcy__stc__scb__i2c__master__xfer__config__t">cy_stc_scb_i2c_master_xfer_config_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The buffer must not be modified and must stay allocated until data has been copied into the TX FIFO.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;&lt;a href="group__group__scb__i2c__master__high__level__functions.html#group__group__scb__i2c__master__high__level__functions_1ga91f7a32d2a20cf3deebe01cef8d9d000"&gt;Cy_SCB_I2C_MasterWrite&lt;/a&gt; requests the SCB hardware to generate a start condition when there is no pending transfer and returns (does not wait until hardware generate a start condition). If the I2C bus is busy the hardware will not generate the until bus becomes free. The SCB hardware sets the busy status after the Start detection, and clears it on the Stop detection. Noise caused by the ESD or other events may cause an erroneous Start condition on the bus. Then, the master will never generate a Start condition because the hardware assumes the bus is busy. If this occurs, the &lt;a href="group__group__scb__i2c__master__high__level__functions.html#group__group__scb__i2c__master__high__level__functions_1ga652a3b9da6db2424fed62c28c6347ef3"&gt;Cy_SCB_I2C_MasterGetStatus&lt;/a&gt; returns &lt;a href="group__group__scb__i2c__macros__master__status.html#group__group__scb__i2c__macros__master__status_1ga412fef33336fb22c95f342ccf2485a0a"&gt;CY_SCB_I2C_MASTER_BUSY&lt;/a&gt; status and the transaction will never finish. The option is to implement a timeout to detect the transfer completion. If the transfer never completes, the SCB needs a reset by calling the &lt;a href="group__group__scb__i2c__general__functions.html#group__group__scb__i2c__general__functions_1ga914462cc121171d82335036e65d6ea78"&gt;Cy_SCB_I2C_Disable&lt;/a&gt; and &lt;a href="group__group__scb__i2c__general__functions.html#group__group__scb__i2c__general__functions_1gad14d83faab69d2ac96feede664df3687"&gt;Cy_SCB_I2C_Enable&lt;/a&gt; functions. The &lt;a href="group__group__scb__i2c__master__high__level__functions.html#group__group__scb__i2c__master__high__level__functions_1ga0815311112e61ca8c1268a6f637dc427"&gt;Cy_SCB_I2C_MasterAbortWrite&lt;/a&gt; function will not work, the block must be reset. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1568" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1471" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="682" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__high__level__functions_1ga0815311112e61ca8c1268a6f637dc427" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_I2C_MasterAbortWrite</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_MasterAbortWrite</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function requests the master to abort write operation by generating a Stop condition. </para>        </briefdescription>
        <detaileddescription>
<para>The function does not wait until this action is completed. Therefore next write operation can be initiated only after the <ref kindref="member" refid="group__group__scb__i2c__macros__master__status_1ga412fef33336fb22c95f342ccf2485a0a">CY_SCB_I2C_MASTER_BUSY</ref> is cleared.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Side Effects</title><para>If the TX FIFO is used, it is cleared before Stop generation. The TX FIFO clear operation also clears shift register. Thus, the shifter could be cleared in the middle of a data element transfer, corrupting it. The remaining bits to transfer within corrupted data element are complemented with ones.<linebreak />
If the clear operation is requested while the master transmits the address, the direction of transaction is changed to read and one byte is read before Stop is issued. This byte is discarded. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1656" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1600" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="684" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__high__level__functions_1ga83185adeefa83640ac09852c9d6cf426" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></type>
        <definition>cy_en_scb_i2c_status_t Cy_SCB_I2C_MasterRead</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_i2c_master_xfer_config_t *xferConfig, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_MasterRead</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__master__xfer__config__t">cy_stc_scb_i2c_master_xfer_config_t</ref> *</type>
          <declname>xferConfig</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function configures the master to automatically read an entire buffer of data from the slave device. </para>        </briefdescription>
        <detaileddescription>
<para>After the transaction is initiated by this function it returns and <ref kindref="member" refid="group__group__scb__i2c__interrupt__functions_1ga729b2fa4de4d44ea2e8995d7ca6a0c24">Cy_SCB_I2C_Interrupt</ref> manages further data transfer.</para><para>When a read transaction is completed (requested number of bytes are read or error occurred) the <ref kindref="member" refid="group__group__scb__i2c__macros__master__status_1ga412fef33336fb22c95f342ccf2485a0a">CY_SCB_I2C_MASTER_BUSY</ref> status is cleared and the <ref kindref="member" refid="group__group__scb__i2c__macros__callback__events_1ga692e9cdd3ec1afa5950905558dc915d3">CY_SCB_I2C_MASTER_RD_CMPLT_EVENT</ref> event is generated.</para><para>Note that the master must read at least one byte.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>xferConfig</parametername>
</parameternamelist>
<parameterdescription>
<para>Master transfer configuration structure <ref kindref="compound" refid="structcy__stc__scb__i2c__master__xfer__config__t">cy_stc_scb_i2c_master_xfer_config_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;The buffer must not be modified and must stay allocated until read operation completion.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;&lt;a href="group__group__scb__i2c__master__high__level__functions.html#group__group__scb__i2c__master__high__level__functions_1ga83185adeefa83640ac09852c9d6cf426"&gt;Cy_SCB_I2C_MasterRead&lt;/a&gt; requests the SCB hardware to generate a start condition when there is no pending transfer and returns (does not wait until hardware generate a start condition). If the I2C bus is busy the hardware will not generate the until bus becomes free. The SCB hardware sets the busy status after the Start detection, and clears it on the Stop detection. Noise caused by the ESD or other events may cause an erroneous Start condition on the bus. Then, the master will never generate a Start condition because the hardware assumes the bus is busy. If this occurs, the &lt;a href="group__group__scb__i2c__master__high__level__functions.html#group__group__scb__i2c__master__high__level__functions_1ga652a3b9da6db2424fed62c28c6347ef3"&gt;Cy_SCB_I2C_MasterGetStatus&lt;/a&gt; returns &lt;a href="group__group__scb__i2c__macros__master__status.html#group__group__scb__i2c__macros__master__status_1ga412fef33336fb22c95f342ccf2485a0a"&gt;CY_SCB_I2C_MASTER_BUSY&lt;/a&gt; status and the transaction will never finish. The option is to implement a timeout to detect the transfer completion. If the transfer never completes, the SCB needs a reset by calling the &lt;a href="group__group__scb__i2c__general__functions.html#group__group__scb__i2c__general__functions_1ga914462cc121171d82335036e65d6ea78"&gt;Cy_SCB_I2C_Disable&lt;/a&gt; and &lt;a href="group__group__scb__i2c__general__functions.html#group__group__scb__i2c__general__functions_1gad14d83faab69d2ac96feede664df3687"&gt;Cy_SCB_I2C_Enable&lt;/a&gt; functions. The &lt;a href="group__group__scb__i2c__master__high__level__functions.html#group__group__scb__i2c__master__high__level__functions_1gae25688c0c4f7434a6284c452c9ebd897"&gt;Cy_SCB_I2C_MasterAbortRead&lt;/a&gt; function will not work, the block must be reset. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1338" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1243" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="685" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__high__level__functions_1gae25688c0c4f7434a6284c452c9ebd897" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SCB_I2C_MasterAbortRead</definition>
        <argsstring>(CySCB_Type *base, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_MasterAbortRead</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function requests master to abort read operation by NAKing the next byte and generating a Stop condition. </para>        </briefdescription>
        <detaileddescription>
<para>The function does not wait until these actions are completed. Therefore the next operation can be initiated only after the <ref kindref="member" refid="group__group__scb__i2c__macros__master__status_1ga412fef33336fb22c95f342ccf2485a0a">CY_SCB_I2C_MASTER_BUSY</ref> is cleared.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1419" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1360" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="687" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__high__level__functions_1ga652a3b9da6db2424fed62c28c6347ef3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_I2C_MasterGetStatus</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_i2c_context_t const *context)</argsstring>
        <name>Cy_SCB_I2C_MasterGetStatus</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the current I2C master status. </para>        </briefdescription>
        <detaileddescription>
<para>This status is a bit mask and the value returned may have multiple bits set.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="compound" refid="group__group__scb__i2c__macros__master__status">I2C Master Status</ref>. Note that not all I2C master statuses are returned by this function. Refer to more details of each status.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Status is cleared by calling &lt;a href="group__group__scb__i2c__master__high__level__functions.html#group__group__scb__i2c__master__high__level__functions_1ga83185adeefa83640ac09852c9d6cf426"&gt;Cy_SCB_I2C_MasterRead&lt;/a&gt; or &lt;a href="group__group__scb__i2c__master__high__level__functions.html#group__group__scb__i2c__master__high__level__functions_1ga91f7a32d2a20cf3deebe01cef8d9d000"&gt;Cy_SCB_I2C_MasterWrite&lt;/a&gt;. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1189" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1183" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="688" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__high__level__functions_1gae70af483602c1b26248c5a74a72c74c7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_SCB_I2C_MasterGetTransferCount</definition>
        <argsstring>(CySCB_Type const *base, cy_stc_scb_i2c_context_t const *context)</argsstring>
        <name>Cy_SCB_I2C_MasterGetTransferCount</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> const *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> const *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Returns the number of bytes transferred since the last call of <ref kindref="member" refid="group__group__scb__i2c__master__high__level__functions_1ga91f7a32d2a20cf3deebe01cef8d9d000">Cy_SCB_I2C_MasterWrite</ref> or <ref kindref="member" refid="group__group__scb__i2c__master__high__level__functions_1ga83185adeefa83640ac09852c9d6cf426">Cy_SCB_I2C_MasterRead</ref> function. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Number of bytes read or written by the master.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;This function returns an invalid value if read or write transaction was aborted or any listed event occurs during the transaction: &lt;a href="group__group__scb__i2c__macros__master__status.html#group__group__scb__i2c__macros__master__status_1ga1ce9a39fdccdb63d47aea3cc048ffb90"&gt;CY_SCB_I2C_MASTER_ARB_LOST&lt;/a&gt;, &lt;a href="group__group__scb__i2c__macros__master__status.html#group__group__scb__i2c__macros__master__status_1ga8eeded717a61477c8f06c59c00ef3f59"&gt;CY_SCB_I2C_MASTER_BUS_ERR&lt;/a&gt; or &lt;a href="group__group__scb__i2c__macros__master__status.html#group__group__scb__i2c__macros__master__status_1ga39e4ddb9e5a4b11b74f10ea893bf44c2"&gt;CY_SCB_I2C_MASTER_ABORT_START&lt;/a&gt;.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;This number is updated only when the transaction completes, either through an error or successfully. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1694" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1688" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="689" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>