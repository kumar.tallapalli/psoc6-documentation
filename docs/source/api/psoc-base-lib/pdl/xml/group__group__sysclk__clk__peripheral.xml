<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sysclk__clk__peripheral" kind="group">
    <compoundname>group_sysclk_clk_peripheral</compoundname>
    <title>Peripherals Clock Dividers</title>
    <innergroup refid="group__group__sysclk__clk__peripheral__funcs">Functions</innergroup>
    <innergroup refid="group__group__sysclk__clk__peripheral__enums">Enumerated Types</innergroup>
    <briefdescription>
<para>There are multiple peripheral clock dividers that, in effect, create multiple separate peripheral clocks. </para>    </briefdescription>
    <detaileddescription>
<para>The available dividers vary per device series. As an example, for the PSoC 63 series there are 29 dividers:</para><para><itemizedlist>
<listitem><para>eight 8-bit dividers</para></listitem><listitem><para>sixteen 16-bit dividers</para></listitem><listitem><para>four fractional 16.5-bit dividers (16 integer bits, 5 fractional bits)</para></listitem><listitem><para>one fractional 24.5-bit divider (24 integer bits, 5 fractional bits)</para></listitem></itemizedlist>
</para><para>The 8-bit and 16-bit dividers are integer dividers. A divider value of 1 means the output frequency matches the input frequency (that is, there is no change). Otherwise the frequency is divided by the value of the divider. For example, if the input frequency is 50 MHz, and the divider is value 10, the output frequency is 5 MHz.</para><para>The five fractional bits supports further precision in 1/32nd increments. For example, a divider with an integer value of 3 and a fractional value of 4 (4/32) results in a divider of 3.125. Fractional dividers are useful when a high-precision clock is required, for example, for a UART/SPI serial interface.</para><para><image name="sysclk_peri_divs.png" type="html" />
</para><para>Each peripheral can connect to any one of the programmable dividers. A particular peripheral clock divider can drive multiple peripherals.</para><para>The SysClk driver also supports phase aligning two peripheral clock dividers using <ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1ga2e6e86005279b998b20dc93424c30127">Cy_SysClk_PeriphEnablePhaseAlignDivider()</ref>. Alignment works for both integer and fractional dividers. The divider to which a second divider is aligned must already be enabled. </para>    </detaileddescription>
  </compounddef>
</doxygen>