====================================
Defines to skip the callbacks modes
====================================

.. doxygengroup:: group_syspm_skip_callback_modes
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: