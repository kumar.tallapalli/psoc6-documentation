==============
API Reference
==============

.. raw:: html

   <hr>

The following provides a list of API documentation

.. toctree::
   :hidden:
   
   group__group__board__libs__shield.rst
   group__group__board__libs__pins.rst
   group__group__board__libs__microphone.rst
   group__group__board__libs__motion.rst
   group__group__board__libs__temp.rst
   group__group__board__libs__display.rst


+----------------------------------+------------------------------------------------------------------+
|  \ `Shield <group__gro           | Basic set of APIs for                                            |
| up__board__libs__shield.html>`__ | interacting with the                                             |
|                                  | CY8CKIT-028-EPD shield board                                     |
+----------------------------------+------------------------------------------------------------------+
|  \ `Pins <group__g               | Pin mapping of the GPIOs used by                                 |
| roup__board__libs__pins.html>`__ | shield peripherals                                               |
+----------------------------------+------------------------------------------------------------------+
|  \ `Microphone <group__group__   | The microphone uses the HAL                                      |
| board__libs__microphone.html>`__ | PDM_PCM driver for its                                           |
|                                  | initialization                                                   |
+----------------------------------+------------------------------------------------------------------+
|  \ `Motion                       | The motion sensor is handled by                                  |
| Sensor <group__gro               | the sensor-motion-bmi160                                         |
| up__board__libs__motion.html>`__ | library, details are available                                   |
|                                  | at                                                               |
|                                  | https://github.com/cypresssemiconductorco/sensor-motion-bmi160   |
+----------------------------------+------------------------------------------------------------------+
|  \ `Temperature                  | The temperature sensor is                                        |
| Sensor <group__g                 | handled by the thermistor                                        |
| roup__board__libs__temp.html>`__ | library, details are available                                   |
|                                  | at                                                               |
|                                  | https://github.com/cypresssemiconductorco/thermistor             |
+----------------------------------+------------------------------------------------------------------+
|  \ `E-Ink                        | The display is handled by the                                    |
| Display <group__grou             | display-eink-e2271cs021 library,                                 |
| p__board__libs__display.html>`__ | details are available at                                         |
|                                  | https://github.com/cypresssemiconductorco/display-eink-e2271cs021|
+----------------------------------+------------------------------------------------------------------+


