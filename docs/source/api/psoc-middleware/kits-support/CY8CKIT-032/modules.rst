==============
API Reference
==============

.. raw:: html

   <hr>

.. toctree::
   :hidden:
   
   group__group__board__libs__pins.rst
   group__group__board__libs__afe.rst
   group__group__board__libs__display.rst
   

The following provides a list of API documentation

+----------------------------------+-----------------------------------------------------------------+
|  \ `Pins <group__group__         | Pin mapping of the GPIOs used by                                |
|  board__libs__pins.html>`_       | shield peripherals                                              |
+----------------------------------+-----------------------------------------------------------------+
|  \ `Analog Front End             | APIs for interacting with the                                   |
|  <group__                        | sensors and user interface                                      |
|  group__board__libs__afe.html>`_ | widgets on the board                                            |
+----------------------------------+-----------------------------------------------------------------+
|  \ `OLED Display                 | The display is handled by the                                   |
|  <group__group__                 | display-oled-ssd1306 library,                                   |
|  board__libs__display.html>`_    | details are available at                                        |
|                                  | https://github.com/cypresssemiconductorco/display-oled-ssd1306  |
+----------------------------------+-----------------------------------------------------------------+

