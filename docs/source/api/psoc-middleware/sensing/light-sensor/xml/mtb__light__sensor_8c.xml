<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="mtb__light__sensor_8c" kind="file" language="C++">
    <compoundname>mtb_light_sensor.c</compoundname>
    <includes local="yes" refid="mtb__light__sensor_8h">mtb_light_sensor.h</includes>
    <includes local="yes">cyhal_adc.h</includes>
    <incdepgraph>
      <node id="1">
        <label>cyhal_adc.h</label>
      </node>
      <node id="0">
        <label>mtb_light_sensor.c</label>
        <link refid="mtb__light__sensor_8c" />
        <childnode refid="1" relation="include">
        </childnode>
      </node>
    </incdepgraph>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__board__libs_1gae86fdb18c398b5111501915a83fca1ed" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t mtb_light_sensor_init</definition>
        <argsstring>(mtb_light_sensor_t *light_sensor, cyhal_adc_t *adc_obj, cyhal_gpio_t pin)</argsstring>
        <name>mtb_light_sensor_init</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__light__sensor__t">mtb_light_sensor_t</ref> *</type>
          <declname>light_sensor</declname>
        </param>
        <param>
          <type>cyhal_adc_t *</type>
          <declname>adc_obj</declname>
        </param>
        <param>
          <type>cyhal_gpio_t</type>
          <declname>pin</declname>
        </param>
        <briefdescription>
<para>Initialize the ADC communication with the light sensor. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="out">light_sensor</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a Light Sensor object. The caller must allocate the memory for this object but the init function will initialize its contents. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">adc_obj</parametername>
</parameternamelist>
<parameterdescription>
<para>ADC instance to use for communicating with the light sensor. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">pin</parametername>
</parameternamelist>
<parameterdescription>
<para>The pin connected to the light sensor. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS if properly initialized, else an error indicating what went wrong.</para></simplesect>
<para><verbatim>embed:rst 
.. note::
   The ADC initialization function, cyhal_adc_init(), must be called before this function is called. 
</verbatim></para>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="49" bodyfile="output/libs/COMPONENT_LIGHT/sensor-light/mtb_light_sensor.c" bodystart="41" column="1" file="output/libs/COMPONENT_LIGHT/sensor-light/mtb_light_sensor.c" line="41" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1gae8d7901ac044169d797b646887c661b1" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint8_t</type>
        <definition>uint8_t mtb_light_sensor_light_level</definition>
        <argsstring>(mtb_light_sensor_t *light_sensor)</argsstring>
        <name>mtb_light_sensor_light_level</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__light__sensor__t">mtb_light_sensor_t</ref> *</type>
          <declname>light_sensor</declname>
        </param>
        <briefdescription>
<para>Returns the current light level. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">light_sensor</parametername>
</parameternamelist>
<parameterdescription>
<para>Light sensor instance. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A percentage - a value between 0 (no light) and 100 (maximum measurable light). </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="68" bodyfile="output/libs/COMPONENT_LIGHT/sensor-light/mtb_light_sensor.c" bodystart="55" column="1" file="output/libs/COMPONENT_LIGHT/sensor-light/mtb_light_sensor.c" line="55" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1ga30cf1ecf58b8051c9666f846743f8082" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void mtb_light_sensor_free</definition>
        <argsstring>(mtb_light_sensor_t *light_sensor)</argsstring>
        <name>mtb_light_sensor_free</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__light__sensor__t">mtb_light_sensor_t</ref> *</type>
          <declname>light_sensor</declname>
        </param>
        <briefdescription>
<para>Frees up any resources allocated by the light sensor as part of <ref kindref="member" refid="group__group__board__libs_1gae86fdb18c398b5111501915a83fca1ed">mtb_light_sensor_init()</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">light_sensor</parametername>
</parameternamelist>
<parameterdescription>
<para>Light sensor instance. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="77" bodyfile="output/libs/COMPONENT_LIGHT/sensor-light/mtb_light_sensor.c" bodystart="74" column="1" file="output/libs/COMPONENT_LIGHT/sensor-light/mtb_light_sensor.c" line="74" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/**************************************************************************/</highlight><highlight class="comment">/***</highlight></codeline>
<codeline lineno="2"><highlight class="comment"><sp />*<sp />\file<sp />mtb_light_sensor.c</highlight></codeline>
<codeline lineno="3"><highlight class="comment"><sp />*</highlight></codeline>
<codeline lineno="4"><highlight class="comment"><sp />*<sp />This<sp />file<sp />is<sp />the<sp />implementation<sp />for<sp />the<sp />light<sp />sensor.</highlight></codeline>
<codeline lineno="5"><highlight class="comment"><sp />*</highlight></codeline>
<codeline lineno="6"><highlight class="comment"><sp />*******************************************************************************</highlight></codeline>
<codeline lineno="7"><highlight class="comment"><sp />*<sp />\copyright</highlight></codeline>
<codeline lineno="8"><highlight class="comment"><sp />*<sp />Copyright<sp />2018-2020<sp />Cypress<sp />Semiconductor<sp />Corporation</highlight></codeline>
<codeline lineno="9"><highlight class="comment"><sp />*<sp />SPDX-License-Identifier:<sp />Apache-2.0</highlight></codeline>
<codeline lineno="10"><highlight class="comment"><sp />*</highlight></codeline>
<codeline lineno="11"><highlight class="comment"><sp />*<sp />Licensed<sp />under<sp />the<sp />Apache<sp />License,<sp />Version<sp />2.0<sp />(the<sp />"License");</highlight></codeline>
<codeline lineno="12"><highlight class="comment"><sp />*<sp />you<sp />may<sp />not<sp />use<sp />this<sp />file<sp />except<sp />in<sp />compliance<sp />with<sp />the<sp />License.</highlight></codeline>
<codeline lineno="13"><highlight class="comment"><sp />*<sp />You<sp />may<sp />obtain<sp />a<sp />copy<sp />of<sp />the<sp />License<sp />at</highlight></codeline>
<codeline lineno="14"><highlight class="comment"><sp />*</highlight></codeline>
<codeline lineno="15"><highlight class="comment"><sp />*<sp /><sp /><sp /><sp /><sp />http://www.apache.org/licenses/LICENSE-2.0</highlight></codeline>
<codeline lineno="16"><highlight class="comment"><sp />*</highlight></codeline>
<codeline lineno="17"><highlight class="comment"><sp />*<sp />Unless<sp />required<sp />by<sp />applicable<sp />law<sp />or<sp />agreed<sp />to<sp />in<sp />writing,<sp />software</highlight></codeline>
<codeline lineno="18"><highlight class="comment"><sp />*<sp />distributed<sp />under<sp />the<sp />License<sp />is<sp />distributed<sp />on<sp />an<sp />"AS<sp />IS"<sp />BASIS,</highlight></codeline>
<codeline lineno="19"><highlight class="comment"><sp />*<sp />WITHOUT<sp />WARRANTIES<sp />OR<sp />CONDITIONS<sp />OF<sp />ANY<sp />KIND,<sp />either<sp />express<sp />or<sp />implied.</highlight></codeline>
<codeline lineno="20"><highlight class="comment"><sp />*<sp />See<sp />the<sp />License<sp />for<sp />the<sp />specific<sp />language<sp />governing<sp />permissions<sp />and</highlight></codeline>
<codeline lineno="21"><highlight class="comment"><sp />*<sp />limitations<sp />under<sp />the<sp />License.</highlight></codeline>
<codeline lineno="22"><highlight class="comment"><sp />*******************************************************************************/</highlight><highlight class="normal" /></codeline>
<codeline lineno="23"><highlight class="normal" /></codeline>
<codeline lineno="24"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"mtb_light_sensor.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="25"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_adc.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="26"><highlight class="normal" /></codeline>
<codeline lineno="27"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal">{</highlight></codeline>
<codeline lineno="30"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal" /><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="34"><highlight class="comment"><sp />*<sp />Initializes<sp />an<sp />ADC<sp />channel<sp />to<sp />use<sp />with<sp />the<sp />light<sp />sensor.<sp />If<sp />no<sp />ADC</highlight></codeline>
<codeline lineno="35"><highlight class="comment"><sp />*<sp />channel<sp />is<sp />available<sp />(i.e.<sp />reserved<sp />by<sp />the<sp />hardware<sp />resource</highlight></codeline>
<codeline lineno="36"><highlight class="comment"><sp />*<sp />manager),<sp />then<sp />this<sp />initialization<sp />fails.</highlight></codeline>
<codeline lineno="37"><highlight class="comment"><sp />*</highlight></codeline>
<codeline lineno="38"><highlight class="comment"><sp />*<sp />NOTE:<sp />The<sp />ADC<sp />initialization<sp />function,<sp />cyhal_adc_init(),<sp />must<sp />be<sp />called</highlight></codeline>
<codeline lineno="39"><highlight class="comment"><sp />*<sp />before<sp />this<sp />function<sp />is<sp />called.</highlight></codeline>
<codeline lineno="40"><highlight class="comment">*******************************************************************************/</highlight><highlight class="normal" /></codeline>
<codeline lineno="41"><highlight class="normal">cy_rslt_t<sp /><ref kindref="member" refid="group__group__board__libs_1gae86fdb18c398b5111501915a83fca1ed">mtb_light_sensor_init</ref>(<ref kindref="compound" refid="structmtb__light__sensor__t">mtb_light_sensor_t</ref><sp />*light_sensor,<sp />cyhal_adc_t<sp />*adc_obj,</highlight></codeline>
<codeline lineno="42"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cyhal_gpio_t<sp />pin)</highlight></codeline>
<codeline lineno="43"><highlight class="normal">{</highlight></codeline>
<codeline lineno="44"><highlight class="normal"><sp /><sp /><sp /><sp />cy_rslt_t<sp />result;</highlight></codeline>
<codeline lineno="45"><highlight class="normal" /></codeline>
<codeline lineno="46"><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_adc_channel_init(&amp;light_sensor-&gt;<ref kindref="member" refid="structmtb__light__sensor__t_1a7582a4ade59d1850b2e7653bd1e64fbd">channel</ref>,<sp />adc_obj,<sp />pin);</highlight></codeline>
<codeline lineno="47"><highlight class="normal" /></codeline>
<codeline lineno="48"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />result;</highlight></codeline>
<codeline lineno="49"><highlight class="normal">}</highlight></codeline>
<codeline lineno="50"><highlight class="normal" /></codeline>
<codeline lineno="51"><highlight class="normal" /></codeline>
<codeline lineno="52"><highlight class="normal" /><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="53"><highlight class="comment"><sp />*<sp />Returns<sp />the<sp />current<sp />light<sp />level<sp />as<sp />a<sp />percentage.</highlight></codeline>
<codeline lineno="54"><highlight class="comment">*******************************************************************************/</highlight><highlight class="normal" /></codeline>
<codeline lineno="55"><highlight class="normal">uint8_t<sp /><ref kindref="member" refid="group__group__board__libs_1gae8d7901ac044169d797b646887c661b1">mtb_light_sensor_light_level</ref>(<ref kindref="compound" refid="structmtb__light__sensor__t">mtb_light_sensor_t</ref><sp />*light_sensor)</highlight></codeline>
<codeline lineno="56"><highlight class="normal">{</highlight></codeline>
<codeline lineno="57"><highlight class="normal"><sp /><sp /><sp /><sp />uint16_t<sp />adc_reading;</highlight></codeline>
<codeline lineno="58"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />percentage;</highlight></codeline>
<codeline lineno="59"><highlight class="normal" /></codeline>
<codeline lineno="60"><highlight class="normal"><sp /><sp /><sp /><sp />adc_reading<sp />=<sp />cyhal_adc_read_u16(&amp;light_sensor-&gt;<ref kindref="member" refid="structmtb__light__sensor__t_1a7582a4ade59d1850b2e7653bd1e64fbd">channel</ref>);</highlight></codeline>
<codeline lineno="61"><highlight class="normal" /></codeline>
<codeline lineno="62"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">//percentage<sp />=<sp />((uint32_t)adc_reading<sp />*<sp />100)<sp />/<sp />CYHAL_ADC_MAX_VALUE;</highlight><highlight class="normal" /></codeline>
<codeline lineno="63"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">//<sp />This<sp />is<sp />approximately<sp />the<sp />same<sp />as<sp />the<sp />line<sp />above<sp />and<sp />it<sp />doesn't</highlight><highlight class="normal" /></codeline>
<codeline lineno="64"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">//<sp />require<sp />a<sp />divide<sp />operation.</highlight><highlight class="normal" /></codeline>
<codeline lineno="65"><highlight class="normal"><sp /><sp /><sp /><sp />percentage<sp />=<sp />(((uint32_t)adc_reading<sp />+<sp />1)<sp />*<sp />100)<sp />&gt;&gt;<sp />CYHAL_ADC_BITS;</highlight></codeline>
<codeline lineno="66"><highlight class="normal" /></codeline>
<codeline lineno="67"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />(uint8_t)percentage;</highlight></codeline>
<codeline lineno="68"><highlight class="normal">}</highlight></codeline>
<codeline lineno="69"><highlight class="normal" /></codeline>
<codeline lineno="70"><highlight class="normal" /></codeline>
<codeline lineno="71"><highlight class="normal" /><highlight class="comment">/*******************************************************************************</highlight></codeline>
<codeline lineno="72"><highlight class="comment"><sp />*<sp />Free<sp />the<sp />resources<sp />used<sp />with<sp />the<sp />light<sp />sensor.</highlight></codeline>
<codeline lineno="73"><highlight class="comment">*******************************************************************************/</highlight><highlight class="normal" /></codeline>
<codeline lineno="74"><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp /><ref kindref="member" refid="group__group__board__libs_1ga30cf1ecf58b8051c9666f846743f8082">mtb_light_sensor_free</ref>(<ref kindref="compound" refid="structmtb__light__sensor__t">mtb_light_sensor_t</ref><sp />*light_sensor)</highlight></codeline>
<codeline lineno="75"><highlight class="normal">{</highlight></codeline>
<codeline lineno="76"><highlight class="normal"><sp /><sp /><sp /><sp />cyhal_adc_channel_free(&amp;light_sensor-&gt;<ref kindref="member" refid="structmtb__light__sensor__t_1a7582a4ade59d1850b2e7653bd1e64fbd">channel</ref>);</highlight></codeline>
<codeline lineno="77"><highlight class="normal">}</highlight></codeline>
<codeline lineno="78"><highlight class="normal" /></codeline>
<codeline lineno="79"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="80"><highlight class="normal">}</highlight></codeline>
<codeline lineno="81"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_LIGHT/sensor-light/mtb_light_sensor.c" />
  </compounddef>
</doxygen>