==============================
cy_stc_csdadc_config_t Struct
==============================

.. doxygenstruct:: cy_stc_csdadc_config_t
   :project: csdadc

Public Attribute
-----------------

.. doxygenvariable:: ptrPinList
   :project: csdadc

.. doxygenvariable:: base
   :project: csdadc

.. doxygenvariable:: csdCxtPtr
   :project: csdadc

.. doxygenvariable:: cpuClk
   :project: csdadc

.. doxygenvariable:: periClk
   :project: csdadc

.. doxygenvariable:: vref
   :project: csdadc

.. doxygenvariable:: vdda
   :project: csdadc

.. doxygenvariable:: calibrInterval
   :project: csdadc

.. doxygenvariable:: range
   :project: csdadc

.. doxygenvariable:: resolution
   :project: csdadc

.. doxygenvariable:: periDivTyp
   :project: csdadc

.. doxygenvariable:: numChannels
   :project: csdadc

.. doxygenvariable:: idac
   :project: csdadc

.. doxygenvariable:: operClkDivider
   :project: csdadc

.. doxygenvariable:: azTime
   :project: csdadc

.. doxygenvariable:: acqTime
   :project: csdadc

.. doxygenvariable:: csdInitTime
   :project: csdadc

.. doxygenvariable:: idacCalibrationEn
   :project: csdadc

.. doxygenvariable:: periDivInd
   :project: csdadc

