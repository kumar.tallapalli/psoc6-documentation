<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__board__libs" kind="group">
    <compoundname>group_board_libs</compoundname>
    <title>Motion Sensor</title>
    <innerclass prot="public" refid="structmtb__bmi160__t">mtb_bmi160_t</innerclass>
    <innerclass prot="public" refid="structmtb__bmi160__data__t">mtb_bmi160_data_t</innerclass>
      <sectiondef kind="enum">
      <memberdef id="group__group__board__libs_1gaa1ef72a83807270481aaa556892729bd" kind="enum" prot="public" static="no" strong="no">
        <type />
        <name>mtb_bmi160_address_t</name>
        <enumvalue id="group__group__board__libs_1ggaa1ef72a83807270481aaa556892729bdaaa877d4dff6976b545fa97b7ef568ab5" prot="public">
          <name>MTB_BMI160_DEFAULT_ADRESS</name>
          <initializer>= BMI160_I2C_ADDR</initializer>
          <briefdescription>
          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <enumvalue id="group__group__board__libs_1ggaa1ef72a83807270481aaa556892729bda777ff185db7fb2b09486e898223c07e1" prot="public">
          <name>MTB_BMI160_SECONDARY_ADRESS</name>
          <initializer>= BMI160_AUX_BMM150_I2C_ADDR</initializer>
          <briefdescription>
          </briefdescription>
          <detaileddescription>
          </detaileddescription>
        </enumvalue>
        <briefdescription>
<para><bold>mtb_bmi160_address_t: </bold><linebreak />Enumeration used for selecting I2C address.</para></briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="89" bodyfile="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" bodystart="85" column="1" file="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" line="86" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__board__libs_1gac8f26d8dfa22eaef5a39303409133dfc" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t mtb_bmi160_init_i2c</definition>
        <argsstring>(mtb_bmi160_t *obj, cyhal_i2c_t *inst, mtb_bmi160_address_t address)</argsstring>
        <name>mtb_bmi160_init_i2c</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__bmi160__t">mtb_bmi160_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type>cyhal_i2c_t *</type>
          <declname>inst</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__board__libs_1gaa1ef72a83807270481aaa556892729bd">mtb_bmi160_address_t</ref></type>
          <declname>address</declname>
        </param>
        <briefdescription>
<para>Initialize the IMU for I2C communication. </para>        </briefdescription>
        <detaileddescription>
<para>Then applies the default configuration settings for both the accelerometer &amp; gyroscope. Known maximum I2C frequency of 1MHz; refer to manufacturer's datasheet for confirmation. See: <ref kindref="member" refid="group__group__board__libs_1gae39ac49787bfd21ab9c2b1ebdfe03fd8">mtb_bmi160_config_default()</ref> <parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a BMI160 object. The caller must allocate the memory for this object but the init function will initialize its contents. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">inst</parametername>
</parameternamelist>
<parameterdescription>
<para>I2C instance to use for communicating with the BMI160 sensor. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">address</parametername>
</parameternamelist>
<parameterdescription>
<para>BMI160 I2C address, set by hardware implemntation. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS if properly initialized, else an error indicating what went wrong. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="90" bodyfile="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.c" bodystart="70" column="1" file="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" line="106" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1gae39ac49787bfd21ab9c2b1ebdfe03fd8" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t mtb_bmi160_config_default</definition>
        <argsstring>(mtb_bmi160_t *obj)</argsstring>
        <name>mtb_bmi160_config_default</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__bmi160__t">mtb_bmi160_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Configure the motion sensor to a default mode with both accelerometer &amp; gyroscope enabled with a nominal output data rate. </para>        </briefdescription>
        <detaileddescription>
<para>The default values used are from the example in the BMI160 driver repository, see <ulink url="https://github.com/BoschSensortec/BMI160_driver">https://github.com/BoschSensortec/BMI160_driver</ulink> <parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a BMI160 object. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS if properly initialized, else an error indicating what went wrong. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="116" bodyfile="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.c" bodystart="92" column="1" file="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" line="115" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1ga6e259c8f5f575e06fc3d35c525b4038f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t mtb_bmi160_read</definition>
        <argsstring>(mtb_bmi160_t *obj, mtb_bmi160_data_t *sensor_data)</argsstring>
        <name>mtb_bmi160_read</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__bmi160__t">mtb_bmi160_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structmtb__bmi160__data__t">mtb_bmi160_data_t</ref> *</type>
          <declname>sensor_data</declname>
        </param>
        <briefdescription>
<para>Reads the current accelerometer &amp; gyroscope data from the motion sensor. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a BMI160 object. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="out">sensor_data</parametername>
</parameternamelist>
<parameterdescription>
<para>The accelerometer &amp; gyroscope data read from the motion sensor </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS if properly initialized, else an error indicating what went wrong. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="127" bodyfile="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.c" bodystart="118" column="1" file="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" line="123" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1ga5ea290e0ec118186d1c10b23edfaa98e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>struct bmi160_dev *</type>
        <definition>struct bmi160_dev* mtb_bmi160_get</definition>
        <argsstring>(mtb_bmi160_t *obj)</argsstring>
        <name>mtb_bmi160_get</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__bmi160__t">mtb_bmi160_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Gets access to the base motion sensor data. </para>        </briefdescription>
        <detaileddescription>
<para>This allows for direct manipulation of the sensor for any desired behavior. See <ulink url="https://github.com/BoschSensortec/BMI160_driver">https://github.com/BoschSensortec/BMI160_driver</ulink> for more details on the sensor. <parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a BMI160 object. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>pointer to the BMI160 configuration structure. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="132" bodyfile="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.c" bodystart="129" column="1" file="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" line="132" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1gacf88df809c21de6db3fa1b24ba1044f2" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t mtb_bmi160_selftest</definition>
        <argsstring>(mtb_bmi160_t *obj)</argsstring>
        <name>mtb_bmi160_selftest</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__bmi160__t">mtb_bmi160_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Performs both accelerometer and gyro self tests. </para>        </briefdescription>
        <detaileddescription>
<para>Note these tests cause a soft reset of the device and device should be reconfigured after a test. See <ulink url="https://github.com/BoschSensortec/BMI160_driver">https://github.com/BoschSensortec/BMI160_driver</ulink> for more details. <parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a BMI160 object. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS if self tests pass, else an error indicating what went wrong. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="146" bodyfile="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.c" bodystart="134" column="1" file="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" line="141" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1gae3faebe4fcb363ab41d2f79a297ddf66" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>cy_rslt_t</type>
        <definition>cy_rslt_t mtb_bmi160_config_int</definition>
        <argsstring>(mtb_bmi160_t *obj, struct bmi160_int_settg *intsettings, cyhal_gpio_t pin, uint8_t intr_priority, cyhal_gpio_event_t event, cyhal_gpio_event_callback_t callback, void *callback_arg)</argsstring>
        <name>mtb_bmi160_config_int</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__bmi160__t">mtb_bmi160_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <param>
          <type>struct bmi160_int_settg *</type>
          <declname>intsettings</declname>
        </param>
        <param>
          <type>cyhal_gpio_t</type>
          <declname>pin</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>intr_priority</declname>
        </param>
        <param>
          <type>cyhal_gpio_event_t</type>
          <declname>event</declname>
        </param>
        <param>
          <type>cyhal_gpio_event_callback_t</type>
          <declname>callback</declname>
        </param>
        <param>
          <type>void *</type>
          <declname>callback_arg</declname>
        </param>
        <briefdescription>
<para>Configure a GPIO pin as an interrupt for the BMI160. </para>        </briefdescription>
        <detaileddescription>
<para>This configures the pin as an interrupt, and calls the BMI160 interrupt configuration API with the application supplied settings structure. See <ulink url="https://github.com/BoschSensortec/BMI160_driver">https://github.com/BoschSensortec/BMI160_driver</ulink> for more details. <parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a BMI160 object. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">intsettings</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a BMI160 interrupt settings structure. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">pin</parametername>
</parameternamelist>
<parameterdescription>
<para>Which pin to configure as interrupt </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">intr_priority</parametername>
</parameternamelist>
<parameterdescription>
<para>The priority for NVIC interrupt events </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">event</parametername>
</parameternamelist>
<parameterdescription>
<para>The type of interrupt event </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">callback</parametername>
</parameternamelist>
<parameterdescription>
<para>The function to call when the specified event happens. Pass NULL to unregister the handler. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername direction="in">callback_arg</parametername>
</parameternamelist>
<parameterdescription>
<para>Generic argument that will be provided to the callback when called, can be NULL </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RSLT_SUCCESS if interrupt was succesffuly enabled. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="206" bodyfile="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.c" bodystart="174" column="1" file="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" line="157" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__board__libs_1ga8c47f76e2339fd3a4b50d7c8c1462003" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void mtb_bmi160_free</definition>
        <argsstring>(mtb_bmi160_t *obj)</argsstring>
        <name>mtb_bmi160_free</name>
        <param>
          <type><ref kindref="compound" refid="structmtb__bmi160__t">mtb_bmi160_t</ref> *</type>
          <declname>obj</declname>
        </param>
        <briefdescription>
<para>Frees up any resources allocated by the motion_sensor as part of <ref kindref="member" refid="group__group__board__libs_1gac8f26d8dfa22eaef5a39303409133dfc">mtb_bmi160_init_i2c()</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername direction="in">obj</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to a BMI160 object. </para></parameterdescription>
</parameteritem>
</parameterlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="221" bodyfile="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.c" bodystart="208" column="1" file="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" line="163" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="define">
      <memberdef id="group__group__board__libs_1ga6a782574f531667dbb853ad8e068cda8" kind="define" prot="public" static="no">
        <name>CYHAL_BMI160_RSLT_ERR_ADDITIONAL_INT_PIN</name>
        <initializer>(CYHAL_RSLT_CREATE(CY_RSLT_TYPE_ERROR, CY_RSLT_MODULE_BOARD_HARDWARE_BMI160, 0x100))</initializer>
        <briefdescription>
<para>An attempt was made to configure too many gpio pins as interrupts. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" bodystart="92" column="9" file="output/libs/COMPONENT_BMI160/sensor-motion-bmi160/mtb_bmi160.h" line="92" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>Basic set of APIs for interacting with the BMI160 motion sensor. </para>    </briefdescription>
    <detaileddescription>
<para>This provides basic initialization and access to to the basic accelerometer &amp; gyroscope data. It also provides access to the base BMI160 driver for full control. For more information about the motion sensor, see: <ulink url="https://github.com/BoschSensortec/BMI160_driver">https://github.com/BoschSensortec/BMI160_driver</ulink></para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Currently, this library only supports being used for a single instance of this device.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;BMI160 support requires delays. If the RTOS_AWARE component is set or CY_RTOS_AWARE is defined, the HAL driver will defer to the RTOS for delays. Because of this, it is not safe to call any functions other than &lt;a href="group__group__board__libs.html#group__group__board__libs_1gac8f26d8dfa22eaef5a39303409133dfc"&gt;mtb_bmi160_init_i2c&lt;/a&gt; until after the RTOS scheduler has started.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;There is a known issue with the BMI160 endianness detection. Any code referencing the structures defined in the BMI160 driver should have this file first in any includes.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>
<para><heading level="1">Code snippets</heading></para>

<para><bold>Snippet 1: Simple initialization with I2C.</bold></para>
<para>The following snippet initializes an I2C instance and the BMI160, then reads from the BMI160. <programlisting filename="mtb_imu_bmi160_example.c"><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />I2C<sp />and<sp />BMI160<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp />cyhal_i2c_init(&amp;i2c_instance,<sp />CY8CKIT_028_TFT_PIN_IMU_I2C_SDA,<sp />CY8CKIT_028_TFT_PIN_IMU_I2C_SCL,<sp />NULL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__board__libs_1gac8f26d8dfa22eaef5a39303409133dfc">mtb_bmi160_init_i2c</ref>(&amp;imu,<sp />&amp;i2c_instance,<sp />MTB_BMI160_DEFAULT_ADRESS);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Read<sp />data<sp />from<sp />BMI160<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__board__libs_1ga6e259c8f5f575e06fc3d35c525b4038f">mtb_bmi160_read</ref>(&amp;imu,<sp />&amp;imu_data);</highlight></codeline>
</programlisting> </para>

<para><bold>Snippet 2: BMI160 interrupt configuration.</bold></para>
<para>The following snippet demonstrates how to configure a BMI160 interrupt. <programlisting filename="mtb_imu_bmi160_example.c"><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Populate<sp />BMI160<sp />interrupt<sp />configuration<sp />structure<sp />and<sp />configure<sp />interrupt<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">struct<sp /></highlight><highlight class="normal">bmi160_int_settg<sp />intsettings;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cy_rslt_t<sp />result;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_channel<sp />=<sp />BMI160_INT_CHANNEL_1;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_type<sp />=<sp />BMI160_ACC_ANY_MOTION_INT;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_pin_settg.output_en<sp />=<sp />BMI160_ENABLE;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_pin_settg.output_mode<sp />=<sp />0;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_pin_settg.output_type<sp />=<sp />0;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_pin_settg.edge_ctrl<sp />=<sp />1;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_pin_settg.input_en<sp />=<sp />0;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_pin_settg.latch_dur<sp />=<sp />0;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_type_cfg.acc_any_motion_int.anymotion_en<sp />=<sp />1;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_type_cfg.acc_any_motion_int.anymotion_x<sp />=<sp />1;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_type_cfg.acc_any_motion_int.anymotion_y<sp />=<sp />1;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_type_cfg.acc_any_motion_int.anymotion_z<sp />=<sp />1;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_type_cfg.acc_any_motion_int.anymotion_dur<sp />=<sp />0;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.int_type_cfg.acc_any_motion_int.anymotion_thr<sp />=<sp />20;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.fifo_full_int_en<sp />=<sp />0;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intsettings.fifo_wtm_int_en<sp />=<sp />0;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />result<sp />=<sp /><ref kindref="member" refid="group__group__board__libs_1gae3faebe4fcb363ab41d2f79a297ddf66">mtb_bmi160_config_int</ref>(&amp;imu,<sp />&amp;intsettings,<sp />CY8CKIT_028_TFT_PIN_IMU_INT_1,<sp />CYHAL_ISR_PRIORITY_DEFAULT,<sp />CYHAL_GPIO_IRQ_FALL,<sp />(cyhal_gpio_event_callback_t)<sp />&amp;imucallback,<sp />NULL);</highlight></codeline>
</programlisting></para>

    </detaileddescription>
  </compounddef>
</doxygen>