================================
Scan Parameters Service (ScPS)
================================

.. doxygengroup:: group_ble_service_api_SCPS
   :project: bless
   
   
.. toctree::
   :hidden:

   group__group__ble__service__api___s_c_p_s__server__client.rst
   group__group__ble__service__api___s_c_p_s__server.rst
   group__group__ble__service__api___s_c_p_s__client.rst
   group__group__ble__service__api___s_c_p_s__definitions.rst