==================================
Indoor Positioning Service (IPS)
==================================

.. doxygengroup:: group_ble_service_api_IPS
   :project: bless

.. toctree::
   :hidden:

   group__group__ble__service__api___i_p_s__server__client.rst
   group__group__ble__service__api___i_p_s__server.rst
   group__group__ble__service__api___i_p_s__client.rst
   group__group__ble__service__api___i_p_s__definitions.rst