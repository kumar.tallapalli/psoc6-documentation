=========================
Human Machine Interface
=========================

.. raw:: html

   <script type="text/javascript">
   window.location.href = "emwin/emwin.html"
   </script>

.. toctree::
   :hidden:

   emwin/emwin.rst
   rgb-led/rgb-led.rst  
   capsense/capsense.rst