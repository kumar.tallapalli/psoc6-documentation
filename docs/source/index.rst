.. PSoC 6 documentation master file, created by
   sphinx-quickstart on Tue Sep 15 10:40:11 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PSoC6's documentation!
====================================

.. raw:: html

   <script type="text/javascript">
   window.location.href = "Modustoolbox/Installing_ModusToolbox.html"
   </script>


.. toctree::
   :hidden:
      
   Modustoolbox/Modustoolbox.rst
   psoc6-code-example.rst
   feature-guide/feature-guide.rst
   api/api.rst
   tool-guide/tool-guide.rst 
   Hardware_Reference/Hardware_Reference.rst
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
